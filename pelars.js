var express = require('express'),
    mongoose = require('mongoose'),
    flash = require('connect-flash'),
    ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn;
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var multer = require('multer');
var session = require('express-session');
var mongoStore = require('connect-mongo')(session);
var dbconfig = require('./dbconfig');

//models
var models = require('./models'),
    Scenario,      //Scenario mongoose model
    Activity,      //An activity within a scenario
    CollectedData, //Collected Data mongoose model
    User;          //User mongoose model

//routes
var routes = require('./routes'), //routes
    test = require('./routes/test'),
    authoring = require('./routes/authoring'),
    mobile = require('./routes/mobile'),
    login = require('./routes/login'),
    api = require('./routes/api'),
    registration = require('./routes/registration'),
    visualization = require('./routes/visualization');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

//config for the mongobd connection
var dbconf = {
  db: {
    db: dbconfig.db.db.db
    ,host: dbconfig.db.db.host
    ,port: dbconfig.db.db.port
    //,username: dbconf.db.username
    //,password: dbconf.db.password
  },
  secret: dbconfig.db.secret
  /*db: {
    db: 'mlearn4web_pelars',
    host: 'localhost',
    port: 27017
    //username: 'cm',
    //password: 'cm'
  },
  secret: 'ubersecret'*/
};

// uncomment after placing your favicon in /public
app.use(favicon(__dirname + '/public/img/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
//session support
app.use(session({
  cookie: {
    //maxAge: new Date(Date.now() + 3600000)
  } ,
  //maxAge: new Date(Date.now() + 3600000),
  secret: dbconf.secret ,
  store: new mongoStore(dbconf.db),
  resave: true,
  saveUninitialized: true
}));
app.use(flash());
app.use(login.getPassport().initialize());
app.use(login.getPassport().session());
app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(multer({dest: __dirname + '/public/files'}));

/**
 * Models for the database and exports
 */
models.defineModels(mongoose, function () {
  /*var dbUrl = 'mongodb://';
  //dbUrl += dbconf.db.username+':'+dbconf.db.password+'@';
  dbUrl += dbconf.db.host + ':' + dbconf.db.port;
  dbUrl += '/' + dbconf.db.db;*/
  var dbUrl = dbconfig.db.url;
  app.Scenario = Scenario = mongoose.model('Scenario');
  app.Activity = Activity = mongoose.model('Activity');
  app.CollectedData = CollectedData = mongoose.model('CollectedData');
  app.User = User = mongoose.model('User');
  db = mongoose.connect(dbUrl);
});

global.appRoot = path.resolve(__dirname);

/**
 * exports models to use in other "classes"
 */
exports.newUser = function(params){
  var u = new User(params);
  return u;
};
exports.getScenario = function(){
  var s = Scenario;
  return s;
};
exports.getActivity = function(){
  var a = Activity;
  return a;
};
exports.newActivity = function(params){
  var a = new Activity(params);
  return a;
};
exports.getCollectedData = function(){
  var c = CollectedData;
  return c;
};
exports.getUser = function(){
  var u = User;
  return u;
};

//cross origin accept
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  // intercept OPTIONS method
  if ('OPTIONS' == req.method) {
    res.send(200);
  }
  else {
    next();
  }
});

/**
 * routes
 */
app.get('/', routes.index);

//authoring
app.get('/letsgo4web', ensureLoggedIn('/login'), authoring.scenarios); //the authoring tool
app.post('/letsgo4web/savescenario', authoring.save); //save a scenario as JSON
app.get('/letsgo4web/:sid?', authoring.index); //lets the user chose from a list of scenarios or create one

//visualization
app.get('/visualization/:sid?', ensureLoggedIn('/login'), visualization.index);

//testing purposes
app.get('/test', mobile.test);

//API
//get all scenarios as JSON
app.get('/mlearn4web/getall', api.getall); //get the json data of all scenarios
//get information about a certain scenario as JSON
app.get('/mlearn4web/get/:sid?', api.getscenario); //get the json data of a scenario
//add a new scenario
app.post('/mlearn4web/newscenario', api.newscenario); //create a new scenario
//update a scenario
app.put('/mlearn4web/updatescenario/:sid?', api.updatescenario);
//delete a scenario
app.get('/mlearn4web/deletescenario/:sid?', api.deletescenario);

//get all data as JSON
app.get('/mlearn4web/getalldata', api.getalldata);
//get a certain dataset as JSON
app.get('/mlearn4web/getdata/:did?', api.getdata);
//get all saved data from a certain scenario as JSON
app.get('/mlearn4web/getscenariodata/:sid?', api.getscenariodata); //get the json of the saved data of a scenario
//post a new dataset to a certain scenario
app.post('/mlearn4web/newdata/:sid?', api.newdata);
//update a certain dataset
app.put('/mlearn4web/updatedata/:did?', api.updatedata);
//delete a certain dataset
app.get('/mlearn4web/deletedata/:did?', api.deletedata);

//get all media as JSON
app.get('/mlearn4web/getuserdata/:uid?', api.getuserdata); //get the json of the saved data of a scenario

//get a certain media file
app.get('/mlearn4web/getallmedia', api.getallmedia); //get the json data of all saved media files
//get all saved data as JSON
app.get('/mlearn4web/getmedia/:mid?', api.getmedia); //get the json data of all saved media files
//hide a scenario
app.get('/mlearn4web/hide/:sid?', api.hide); //hides the scenario with the provided id
//unhide a scenario
app.get('/mlearn4web/unhide/:sid?', api.unhide); //unhides the scenario with the provided id
//test for pelars upload
app.put('/mlearn4web/pelars', api.pelars); //unhides the scenario with the provided id


//mobile api
app.get('/getdata', mobile.getdata); //get the json data of a scenario


//registration
app.get('/register', registration.index);
app.post('/register/newuser', registration.register);

//login
login.initializeLogin();

app.get('/login', login.index, routes.index);
app.get('/logout', login.loggedIn, login.logout);
// app.get('/login/failure', login.failure);
// app.get('/login/success', login.success);
app.post('/auth/local', login.local);
// Redirect the user to Facebook for authentication.  When complete, Facebook
// will redirect the user back to the application at
//   /auth/facebook/callback
app.get('/auth/facebook', login.facebook);
// Facebook will redirect the user to this URL after approval.
app.get('/auth/facebook/callback', login.facebookCallback);
// Redirect the user to Twitter for authentication.  When complete, Twitter
// will redirect the user back to the application at
//   /auth/twitter/callback
app.get('/auth/twitter', login.twitter);
// Twitter will redirect the user to this URL after approval.
app.get('/auth/twitter/callback', login.twitterCallback);
// Redirect the user to Google for authentication.  When complete, Google
// will redirect the user back to the application at
//     /auth/google/callback
app.get('/auth/google', login.google);
// Google will redirect the user to this URL after authentication.
app.get('/auth/google/callback', login.googleCallback);

app.get('/testing', test.index);

//app.get('/mobile', mobile.mobile);

app.get('/mobile', mobile.index);
app.get('/mobile_old', mobile.jqmobile);
app.get('/mobile/activity/:aid?', mobile.activity);
app.get('/mobile/activity/:aid?/:screen?', mobile.actscreen);
app.post('/mobile/newActivity', mobile.newActivity);

//mobile
//app.get('/mobile', mobile.index);
app.post('/mobile/savedata', mobile.save); //save the collected data as JSON
app.get('/mobile/showdata', mobile.data); //save the collected data as JSON
app.get('/mobile/:sid?', mobile.mobile_old); //lets the user chose from a list of scenarios or creates a default one


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;

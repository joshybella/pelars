var Scenario;
var CollectedData;
var User;
var Activity;

function extractKeywords(text) {
  if (!text) return [];

  return text.
    split(/\s+/).
    filter(function(v) { return v.length > 2; }).
    filter(function(v, i, a) { return a.lastIndexOf(v) === i; });
}

function defineModels(mongoose, fn) {
  var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

  /**
   * Model: Scenario
   */
  Scenario = new Schema({
    'title': String,
    'type': String,
    'activities': [],
    'description': String,
    'startDate': String,
    'endDate': String,
    'screenData': [ Schema.Types.Mixed ],
    'user': { type: Schema.Types.ObjectId, ref: 'User' },
    'createdAt': { type : Date, default: Date.now  },
    'lastModified': { type : Date, default: Date.now  },
    'hidden': { type: Boolean, default: false }
  });
  Scenario.pre('save', function(next) {
    this.keywords = extractKeywords(this.data);
    next();
  });

  mongoose.model('Scenario', Scenario);

  /**
   * Model: Activity
   */
  Activity = new Schema({
    'title': String,
    'members': [],
    'pelarsLogin': String,
    'pelarsPassword': String,
    'pelarsID': String,
    'screenData': [ Schema.Types.Mixed ],
    'createdAt': { type : Date, default: Date.now  },
    'scenarioId': String, //ID of the Scenario
  });
  Activity.pre('save', function(next) {
    this.keywords = extractKeywords(this.data);
    next();
  });

  mongoose.model('Activity', Activity);

  /**
   * Model: Collected Data
   */
  CollectedData = new Schema({
    'scenarioId': String, //ID of the Scenario
    'activityId': String, //ID of the Activity
    'groupname': String,
    'time': Number,
    'Phase': String,
    'screen': String,
    'scenarioVersion': String,
    'timestamp' : { type : Date, default: Date.now },
    'data': { type: Schema.Types.Mixed },
    'pelarsId': String
  });

  mongoose.model('CollectedData', CollectedData);

  /**
   * Model: User
   */
  User = new Schema({
    'username': String,
    'password': String,
    'oauthID': String
  });

  mongoose.model('User', User);

  fn();
}

exports.defineModels = defineModels;


var letsgo = require('../pelars');

/*
 * GET visualization.
 */
exports.index = function(req, res) {
    var scenarioFromDB = letsgo.getScenario();
    var collectedDataFromDB = letsgo.getCollectedData();
    
    var sId = req.params.sid;

    if (sId) {
	    scenarioFromDB.findOne({ _id: sId }, function(err, scenario) {
  			if (err) return console.error(err);

        if (!scenario) {
          showList(req, res);
          return;
        }

        collectedDataFromDB.find({scenarioId: sId}, function(err, data){
          if (err)
            return console.error(err);
          else {
            var groupnames = [];
            //console.log(data);
            data.forEach(function(entry) {
              if (entry.groupname){
                //console.log("indexOF: "+groupnames.indexOf(entry.groupname));
                if (groupnames.indexOf(entry.groupname) === -1){
                  groupnames.push(entry.groupname);
                  console.log("GR: "+groupnames);
                }
              }
            });
            res.render('visualization', {
              user: req.user,
              scenario: scenario,
              groupnames: groupnames,
              data: data
            });
          }
        });
		  });
	  } else {
      showList(req, res);
	  }
};



function showList(req, res) {
    var scenarioFromDB = letsgo.getScenario();

    var Scenario = letsgo.getScenario();

    //find all scenarios created by the current user
    Scenario.find({ user: req.user._id })
            .sort({'name': 1})
            .exec(function(err, scenarios) { 
              if (err) return console.error(err);
              res.render('visualization_list', {
                  user: req.user,
                  scenarios: scenarios
              });
            });

    // scenarioFromDB.find(function (err, scenarios) {
    //     if (err) return console.error(err);
    //     res.render('visualization_list', {
    //         user: req.user,
    //         scenarios: scenarios
    //     });
    // }); 
};
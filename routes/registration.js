var bcrypt = require('bcryptjs');

var letsgo = require('../pelars');

/*
 * GET login.
 */
exports.index = function(req, res) {
    res.render('registration');
};

exports.register = function(req, res) {
	console.log("Register new User");
	console.log("UN: " + req.body.username);
	console.log("PW: " + req.body.password);
	console.log("PW2: " + req.body.password2);

	var username  = req.body.username,
		password  = req.body.password,
		password2 = req.body.password2;

	var User = letsgo.getUser();

	User.findOne({ username: username }, function (err, user) {
  			if (err) return console.error(err);
  			console.log(user);

            if (user == null) {
                console.log("Username is available.");

                if (password === password2) {
                	console.log("valid password");

                	var hash = bcrypt.hashSync(password, 10);

                	var c = new User({username: username, password: hash});

				    c.save(function(err, savedUser) {
				        if (err){
				            console.log(err);
				            return;
				        } else {
				            console.log('User was created.');
				            //res.redirect('/');
                            req.login(savedUser, function(err) {
                                if (err) { return next(err); }
                                return res.redirect('/letsgo4web');
                            });
                            //res.redirect('/login');
				        }
				    });
                } else {
                	console.log("The passwords did not match. Please try it again.");
                	res.render('registration', 
                				{
                					message: 'The passwords did not match. Please try again.', 
                					username: username
                				});
                }

            } else {
            	console.log("Username is already in use. Please choose another one.");
            	res.render('registration', {message: 'Username is already in use. Please choose another one.'});
            }

  			
		});		
}
/**
 * For testing purposes
 */
var pelars  = require('../pelars');
exports.index = function(req, res) {
  res.render('authoring_test',{

  });
};

exports.mobile = function(req, res) {
  res.render('mobile_test',{

  });
};

exports.jqmobile = function(req, res) {
  var scenarioFromDB = pelars.getScenario();

  var sId = req.params.sid;

  console.log(sId);

  if (sId) {
    scenarioFromDB.findOne({ _id: sId }, function (err, scenario) {
      if (err) return console.error(err);

      if (!scenario) {
        showList(res, 'Scenario not available.');
        return;
      }

      res.render('mobile', {
        scenario: scenario
      });
    });
  } else {
    showList(res);
  }
};

exports.newActivity = function(req, res) {
  var Scenario = pelars.getScenario(),
      Activity = pelars.getActivity();

  var a = new Activity(req.body);
  a.save(function(err){
    if (err){
      console.error(err);
      res.end(err);
    } else {
      console.log("saved "+ a.id);
      res.send(a);
    }
  });
};

exports.activity = function(req, res) {
  var Scenario = pelars.getScenario(),
      Activity = pelars.getActivity();
  var aId = req.params.aid;

  Activity.findById(aId, function(err, activity){
    if (err){
      console.error(err);
      res.end(err);
    } else {
      var sId = activity.scenarioId;
      Scenario.findById(sId, function(err, scenario){
        if (err){
          console.error(err);
          res.end(err);
        } else {
          var screens = scenario.screenData;
          res.render('mobile_act',{
            scenario: scenario,
            activity: activity
          });
          /*res.render('mobile', {
            scenario: scenario
          });*/
        }
      })
    }
  })
};

exports.actscreen = function(req, res) {
  var Scenario = pelars.getScenario(),
      Activity = pelars.getActivity();
  var aId = req.params.aid,
      screen = req.params.screen;

  Activity.findById(aId, function(err, activity){
    if (err){
      console.error(err);
      res.end(err);
    } else {
      var sId = activity.scenarioId;
      Scenario.findById(sId, function(err, scenario){
        if (err){
          console.error(err);
          res.end(err);
        } else {
          var screens = scenario.screenData;
          var newscreens = screens[screen];

          scenario.screenData = newscreens;

          res.render('mobile', {
            activity: activity,
            scenario: scenario
          });
        }
      })
    }
  })
};

function showList(res, message) {
  var scenarioFromDB = pelars.getScenario(),
      activityFromDB = pelars.getActivity();

  scenarioFromDB.find()
    .sort({'lastModified': -1})
    .populate('user', 'username') //get the username of the user who created the scenario
    .exec(function (err, scenarios) {
      if (err) {
        return console.error(err);
        res.end(err);
      } else {
        activityFromDB.find(function(err, activities){
          res.render('mobile_pelarslist', {
            scenarios: scenarios,
            activities: activities,
            message: message
          });
        });
      }
    });
}
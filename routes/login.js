// var FACEBOOK_APP_ID 	 	= '813560605321365',
// 	FACEBOOK_APP_SECRET  	= 'fb391d52618fa0ddbb0ee28fd57f976f',
// 	FACEBOOK_CALLBACK 	 	= 'http://localhost:3000/auth/facebook/callback';

// var TWITTER_CONSUMER_KEY	= 'CQol5K5ESHIZrwgX5Zq7KlvYE',
// 	TWITTER_CONSUMER_SECRET = 'dX2BBm4tqjfnKtVgTNsYhdee3NI0T9Csk4WkKipCbV5Eh3I4l7',
// 	TWITTER_CALLBACK		= 'http://localhost:3000/auth/twitter/callback';

// var GOOGLE_CLIENT_ID		= '630280060569-7bvtaugc7uf4jkpnfc1nr9cqapgog7lu.apps.googleusercontent.com',
//     GOOGLE_CLIENT_SECRET	= 'YR3Fh2almUL3MXgvAS4NoDX0',
// 	GOOGLE_CALLBACK			= 'http://localhost:3000/auth/google/callback';

var bcrypt 			 = require('bcryptjs'),
	  passport 		 = require('passport'),
	  LocalStrategy    = require('passport-local').Strategy,
    FacebookStrategy = require('passport-facebook').Strategy,
    TwitterStrategy  = require('passport-twitter').Strategy,
    GoogleStrategy   = require('passport-google-oauth').OAuth2Strategy;

var letsgo = require('../pelars'),
	oauth  = require('../oauth.js');

exports.getPassport = function() {
    return passport;
}

exports.initializeLogin = function() {
	// console.log("Passport: " + passport);

	// serialize and deserialize
	passport.serializeUser(function(user, done) {
		// console.log('serializeUser: ' + user._id)
	 	done(null, user._id);
	});
	passport.deserializeUser(function(id, done) {
	 	letsgo.getUser().findById(id, function(err, user){
	    	//console.log(user)
	     	if(!err) done(null, user);
	     	else done(err, null)
	 	})
	});


	passport.use(new LocalStrategy(function(username, password, done) {
		console.log("new local strategy");
  		process.nextTick(function() {
  			console.log("next tick");
		    letsgo.getUser().findOne({
		      'username': username,
		    }, function(err, user) {
		    	console.log("find user");
		      	if (err) {
		      		console.log("Error: " + err);
		        	return done(err);
		      	}
		 
		      	if (!user) {
		      		console.log("no user");
		        	return done(null, false, {message: "The user does not exist."});
		      	}
		 		
		      	if (!bcrypt.compareSync(password, user.password)) {
		      		console.log("wrong password");
		        	return done(null, false, {message: "Wrong password."});
		      	}
		 		
		 		console.log("user: " + user);
		      	return done(null, user);
	    	});
		});
	}));

	passport.use(new FacebookStrategy({
		 	clientID: 	  oauth.facebook.appId,
		  	clientSecret: oauth.facebook.appSecret,
		  	callbackURL:  oauth.facebook.callback
		}, function(accessToken, refreshToken, profile, done) {
		  	process.nextTick(function() {
		    	//Assuming user exists
		    	// console.log([profile]);
		    	// profile.username = profile.displayName;
		    	// profile._id = new ObjectID(profile.id);
		    	// done(null, profile);
		    	var User = letsgo.getUser();
		    	User.findOne({ oauthID: profile.id }, function(err, user) {
					if(err) { console.log(err); }
				  	
				  	if (!err && user != null) {
				    	done(null, user);
				  	} else {
				    	var user = new User({
				      		oauthID: profile.id,
				      		username: profile.displayName,
				    	});
				    	user.save(function(err) {
				      		if(err) {
				        		console.log(err);
				      		} else {
				        		console.log("saving facebook user ...");
				        		done(null, user);
				      		};
				    	});
				  	};
				});

		  	});
		}
	));

	passport.use(new TwitterStrategy({
		    consumerKey: 	oauth.twitter.consumerKey,
		    consumerSecret: oauth.twitter.consumerSecret,
		    callbackURL: 	oauth.twitter.callback
		},
		function(token, tokenSecret, profile, done) {
		    // User.findOrCreate(..., function(err, user) {
		    //   if (err) { return done(err); }
		    //   done(null, user);
		    // });
			process.nextTick(function() {
		    	//Assuming user exists
		    	//done(null, profile);

		    	var User = letsgo.getUser();
		    	User.findOne({ oauthID: profile.id }, function(err, user) {
					if(err) { console.log(err); }
				  	
				  	if (!err && user != null) {
				    	done(null, user);
				  	} else {
				    	var user = new User({
				      		oauthID: profile.id,
				      		username: profile.displayName,
				    	});
				    	user.save(function(err) {
				      		if(err) {
				        		console.log(err);
				      		} else {
				        		console.log("saving twitter user ...");
				        		done(null, user);
				      		};
				    	});
				  	};
				});
		  	});
		}
	));

	passport.use(new GoogleStrategy({
	    	clientID: oauth.google.clientId,
    		clientSecret: oauth.google.clientSecret,
    		callbackURL: oauth.google.callback
	  	},
	  	function(accessToken, refreshToken, profile, done) {
	    	// User.findOrCreate({ openId: identifier }, function(err, user) {
	     //  		done(err, user);
	    	// });
			process.nextTick(function() {
		    	//Assuming user exists
		    	//done(null, profile);

		    	var User = letsgo.getUser();
		    	User.findOne({ oauthID: profile.id }, function(err, user) {
					if(err) { console.log(err); }
				  	
				  	if (!err && user != null) {
				    	done(null, user);
				  	} else {
				    	var user = new User({
				      		oauthID: profile.id,
				      		username: profile.displayName,
				    	});
				    	user.save(function(err) {
				      		if(err) {
				        		console.log(err);
				      		} else {
				        		console.log("saving google user ...");
				        		done(null, user);
				      		};
				    	});
				  	};
				});
		  	});
	  	}
	));
	
}


exports.loggedIn = function(req, res, next) {
	if (req.user) {
    	next();
  	} else {
    	res.redirect('/login');
  	}
}

/*
 * GET login.
 */
exports.index = function(req, res) {
    if (req.user) {
    	res.render('login', 
					{
						logout: true
					});	
    } else {
    	//console.log(req.session.messages);
    	res.render('login', { message: req.flash('error')[0] }); //, { message: req.session.messages }
    	//req.session.messages = null;
    }
};

exports.logout = function(req, res) {
	req.logout();
  	res.redirect('/login');
};

// exports.success = function(req, res, next) {
// 	console.log("Successfully authenticated");
// 	//res.send('Successfully authenticated');
	
// 	// next();
// 	res.redirect('/');
// };

// exports.failure = function(req, res, next) {
// 	console.log("Failed to authenticate");
// 	//res.send('Failed to authenticate');
// 	res.redirect('/login');
// };


/************* LOCAL **************/
exports.local = passport.authenticate('local', {
	successReturnToOrRedirect: '/letsgo4web',
	failureRedirect: '/login',
	failureFlash: true
});

/*********** FACEBOOK ************/
exports.facebook = passport.authenticate('facebook');

exports.facebookCallback =  passport.authenticate('facebook', {
	successReturnToOrRedirect: '/letsgo4web',
	failureRedirect: '/login'
});

/*********** TWITTER ************/
exports.twitter = passport.authenticate('twitter');

exports.twitterCallback =  passport.authenticate('twitter', {
	successReturnToOrRedirect: '/letsgo4web',
	failureRedirect: '/login'
});

/************ GOOGLE *************/
exports.google = passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/userinfo.email'] });

exports.googleCallback =  passport.authenticate('google', {
	successReturnToOrRedirect: '/letsgo4web',
	failureRedirect: '/login'
});
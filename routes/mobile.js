var letsgo  = require('../pelars');
var pelars  = require('../pelars');
var http = require('http');
var bcrypt 	= require('bcryptjs'),
    fs      = require('fs'),
    request = require('request');

/*
 * GET mobile.
 */
exports.mobile_old = function(req, res) {
  var scenarioFromDB = letsgo.getScenario(),
      activityFromDB = pelars.getActivity();

  var sId = req.params.sid;

  if (sId) {
    scenarioFromDB.findOne({ _id: sId }, function (err, scenario) {
      if (err) return console.error(err);

      if (!scenario) {
        showList(res, 'Scenario not available.');
        return;
      }

      res.render('mobile', {
        scenario: scenario
      });
    });
  } else {
    showList(res);
  }
};

exports.index = function(req, res) {
  var fileName = appRoot+"/public/ionic/index.html";

  res.sendFile(fileName, function (err) {
    if (err) {
      console.log(err);
      res.status(err.status).end();
    }
    else {
      console.log('Sent:', fileName);
    }
  });

  //res.render('mobile_ionic');
};

exports.data = function(req, res) {
  var collectedDataFromDB = letsgo.getCollectedData();

  collectedDataFromDB.find(function (err, collectedData) {
    if (err) return console.error(err);
    res.end('' + collectedData);
  });
};

exports.save = function(req, res) {
  var scenarioFromDB = letsgo.getScenario(),
      activityFromDB = pelars.getActivity();
  var origin = "";

  if (req.body.model){
    origin = "media";
  } else if (req.body.origin){
    origin = "pelars"
  }

  //console.log(origin);
  //console.log(req.body);

  if (origin == "media"){ //post a media file
    var request = JSON.parse(req.body.model);
    var resp = {
      filename: "/files/"+req.files.file.name,
      type: request.type
    };
    res.send(resp);
    //res.send(req.files.name);
  } else if (origin == "pelars"){ //post from the new PELARS mobile app
    var scenarioId = req.body.scenario;

    var screennr = req.body.activity;
    var groupname = req.body.groupname;
    var pelarsId = req.body.pelarsId;

    var data = {
      'scenarioId': scenarioId,
      'groupname': groupname,
      'screen': screennr,
      'data': {},
      'pelarsId': pelarsId
    };

    scenarioFromDB.findById(scenarioId, function(err, scenario) {
      if (err) {
        console.error(err);
        res.send(err);
      } else {
        data.scenarioVersion = scenario.__v;
        var scrdata = [];


        for (ele in req.body.data){
          switch (ele){
            case "text":
              for (e in req.body.data[ele]){
                var tmp = {};
                tmp.type = "textarea";
                tmp.value = req.body.data[ele][e];
                tmp.elementId = "ta"+e;
                scrdata.push(tmp);
              }
              break;
            case "choice":
              for (e in req.body.data[ele]){
                var tmp = {};
                tmp.type = "choice";
                tmp.value = req.body.data[ele][e];
                tmp.elementId = "cb"+e;
                scrdata.push(tmp);
              }
              break;
            case "files":
              for (e in req.body.data[ele]){
                console.log(req.body.data[ele][e]);
                var tmp = {};
                tmp.type = req.body.data[ele][e]["type"];
                tmp.value = req.body.data[ele][e]["path"]["filename"];
                switch (req.body.data[ele][e]["type"]){
                  case "image":
                    tmp.elementId = "ci"+e;
                    break;
                  case "video":
                    tmp.elementId = "cv"+e;
                    break;
                  case "sound":
                    tmp.elementId = "ca"+e;
                    break;
                }
                scrdata.push(tmp);
              }
              break;
          }
        }

        if (screennr == 0){
          data.data.screen1 = scrdata;
          data.data.screen2 = [];
          data.data.screen3 = [];
        } else if (screennr == 1){
          data.data.screen1 = [];
          data.data.screen2 = scrdata;
          data.data.screen3 = [];
        } else if (screennr == 2){
          data.data.screen1 = [];
          data.data.screen2 = [];
          data.data.screen3 = scrdata;
        } else if (screennr == 3){
          data.data.screen1 = [];
          data.data.screen2 = [];
          data.data.screen3 = [];
          data.data.screen4 = scrdata;
        }
        //console.log(scrdata);

        var CollectedData = letsgo.getCollectedData(),
            c = new CollectedData(data);

        c.save(function(err) {
          if (err){
            console.error(err);
            console.log('Collected Pelars Data save failed');
            res.end('Collected Pelars Data saved failed');
            return;
          } else {
            console.log('Collected Pelars Data saved');
            res.end("end");
          }
        });


      }
    });

    
    
    
  } else { // "traditional post"
    console.log("non pelars");
    var temp = req.headers.referer.split("/");
    var activityId = temp[temp.length-2];
    var screennr = temp[temp.length-1];

    //console.log(req.files);
    console.log(temp);

    activityFromDB.findById(activityId, function(err, activity){
      if(err){
        console.error(err);
        res.end(err);
      } else {
        var scenarioId = activity.scenarioId;
        scenarioFromDB.findById(scenarioId, function(err, scenario){
          if (err){
            console.error(err);
            res.send(err);
          } else {
            var data = {
              'scenarioId': '',
              'groupname': '',
              'data': {}
            };

            for (var element in req.body) {
              if (element === 'scenarioid') {
                data['scenarioId'] = req.body['scenarioid'];
              } else if (element === 'gn') {
                data['groupname'] = req.body['gn'];
              } else if (element === 'pelarsid'){
                data['pelarsid'] = req.body['pelarsid'];
              } else if (element === 'pelarslogin'){
                data['pelarslogin'] = req.body['pelarslogin'];
              } else if (element === 'pelarspassword'){
                data['pelarspassword'] = req.body['pelarspassword'];
              }
              else {
                var parts = element.split('_');
                var screenId = parts[0];
                if (!data.data[screenId]) {
                  data.data[screenId] = [];
                }

                var dataElement = {
                  'elementId': parts[1],
                  'type': '',
                  'value': ''
                }

                var elem = parts[1].substring(0,2);

                var type;
                switch (elem) {
                  case 'tf':
                    type = 'textfield';
                    break;
                  case 'nf':
                    type = 'numerical';
                    break;
                  case 'df':
                    type = 'date';
                    break;
                  case 'ta':
                    type = 'textarea';
                    break;
                  case 'cb':
                    type = 'choice';
                    break;
                  case 'cl':
                    type = 'location';
                    break;
                  case 'co':
                    type = 'orientation';
                    break;
                  case 'qr':
                    type = 'qrreader';
                    break;
                  case 'gn':
                    type = 'groupname';
                    break;
                  default:
                    type = 'unknown';
                    break;
                }

                dataElement.type = type;
                dataElement.value = req.body[element];

                data.data[screenId].push(dataElement);
              }
            }

            for (var element in req.files) {
              var parts = element.split('_');
              var screenId = parts[0];
              if (!data.data[screenId]) {
                data.data[screenId] = [];
              }


              var dataElement = {
                'elementId': parts[1],
                'type': '',
                'value': ''
              }

              var elem = parts[1].substring(0,2);

              var path = req.files[element].path;
              var indexPublic = path.indexOf('public/');
              if (indexPublic > 0) {
                path = path.substring(indexPublic + 6);
              }

              var type;
              switch (elem) {
                case 'ci':
                  type = 'image';
                  break;
                case 'cv':
                  type = 'video';
                  break;
                case 'ca':
                  type = 'sound';
                  break;
                default:
                  type = 'unknown';
                  break;
              }

              dataElement.type = type;
              dataElement.value = path;

              data.data[screenId].push(dataElement);
            }


            if (screennr == 0){
              data.data.screen2 = [];
              data.data.screen3 = [];
            } else if (screennr == 1){
              var tmp = data.data.screen1;
              data.data.screen1 = [];
              data.data.screen2 = tmp;
              data.data.screen3 = [];
            } else if (screennr == 2){
              var tmp = data.data.screen1;
              data.data.screen1 = [];
              data.data.screen2 = [];
              data.data.screen3 = tmp;
            }



            var CollectedData = letsgo.getCollectedData(),
              c = new CollectedData(data);

            c.screen = screennr;

            c.scenarioVersion = scenario.__v;
            c.activityId = activityId;

            c.save(function(err) {
              if (err){
                console.error(err);
                console.log('Collected Data save failed');
                res.end('Collected Data saved failed');
                return;
              } else {
                console.log('Collected Data saved');
                console.log(c);
                var pelars = [data.pelarslogin,data.pelarspassword,data.pelarsid];
                //sendToPelars(c, pelars, req.sessionID); //send the data to the PELARS server

                activity.pelarsLogin = data.pelarslogin;
                activity.pelarsPassword = data.pelarspassword;
                activity.pelarsID = data.pelarsid;
                activity.save(function(err){
                  if (err){
                    console.error(err);
                  }
                });

                showList(res, 'Data was successfully submitted.');
              }
            });
          }
        });
      }
    });

  }



};

/*
 * Call to get the json of a certain scenario (req.params.sname).
 */
exports.getdata = function(req, res) {
  var scenarioFromDB = letsgo.getScenario();
  var sName = req.params.sname;

  scenarioFromDB.find(function (err, scenarios) {
    res.json(scenarios);
  });
  /*
   scenarioFromDB.findOne({ title: sName }, function (err, scenario) {
   res.json(scenario);
   });*/
};

exports.test = function(req, res) {
  res.render('test',{
    user: req.user
  });
};

exports.jqmobile = function(req, res) {
  var scenarioFromDB = pelars.getScenario(),
      activityFromDB = pelars.getActivity();

  var sId = req.params.sid;



  if (sId) {
    scenarioFromDB.findOne({ _id: sId }, function (err, scenario) {
      if (err) return console.error(err);

      if (!scenario) {
        showList(res, 'Scenario not available.');
        return;
      }

      res.render('mobile', {
        scenario: scenario
      });
    });
  } else {
    showList(res);
  }
};

exports.newActivity = function(req, res) {
  var Scenario = pelars.getScenario(),
      Activity = pelars.getActivity();

  var a = new Activity(req.body);
  a.save(function(err){
    if (err){
      console.error(err);
      res.end(err);
    } else {
      console.log("saved "+ a.id);
      res.send(a);
    }
  });
};

exports.activity = function(req, res) {
  var Scenario = pelars.getScenario(),
      Activity = pelars.getActivity(),
      CollectedData = pelars.getCollectedData();
  var aId = req.params.aid;

  Activity.findById(aId, function(err, activity){
    if (err){
      console.error(err);
      res.end(err);
    } else {
      var sId = activity.scenarioId;
      Scenario.findById(sId, function(err, scenario){
        if (err){
          console.error(err);
          res.end(err);
        } else {
          CollectedData.find({'activityId':aId}, function(err,coldata){
            if (err){
              console.error(err);
              res.end(err);
            } else {
              res.render('mobile_act',{
                coldata: coldata,
                scenario: scenario,
                activity: activity
              });
            }

          });
        }
      })
    }
  })
};

exports.actscreen = function(req, res) {
  var Scenario = pelars.getScenario(),
      Activity = pelars.getActivity(),
      CollectedData = pelars.getCollectedData();
  var aId = req.params.aid,
    screen = req.params.screen;

  Activity.findById(aId, function(err, activity){
    if (err){
      console.error(err);
      res.end(err);
    } else {
      var sId = activity.scenarioId;
      Scenario.findById(sId, function(err, scenario){
        if (err){
          console.error(err);
          res.end(err);
        } else {
          var screens = scenario.screenData;
          var newscreens = screens[screen];

          scenario.screenData = newscreens;

          CollectedData.find({'activityId':aId}, function(err, coldata){
            if (err){
              console.error(err);
              res.end(err);
            } else {
              var groupname = "";

              coldata.forEach(function(item){
                if (item.groupname){
                  groupname = item.groupname;
                }
              });
              res.render('mobile', {
                activity: activity,
                scenario: scenario,
                groupname: groupname
              });
            }
          });
        }
      })
    }
  })
};

function showList(res, message) {
  var scenarioFromDB = pelars.getScenario(),
    activityFromDB = pelars.getActivity();

  scenarioFromDB.find()
    .sort({'lastModified': -1})
    .populate('user', 'username') //get the username of the user who created the scenario
    .exec(function (err, scenarios) {
      if (err) {
        return console.error(err);
        res.end(err);
      } else {
        activityFromDB.find(function(err, activities){
          res.render('mobile_pelarslist', {
            scenarios: scenarios,
            activities: activities,
            message: message
          });
        });
      }
    });
}

function sendToPelars(data, pelars, sessionID){
  var groupname = data.groupname;
  var files = [];
  //var file = appRoot+"/public"+data.data.screen1[0].value;
  var token = "";

  var base64Image = "";
  var pelarslogin = pelars[0],
      pelarspwd = pelars[1],
      pelarsid = pelars[2];
  request.post('http://pelars.sssup.it:8080/pelars/password?user='+pelarslogin+'&pwd='+pelarspwd, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var info = JSON.parse(body);
      token = info.token;
      console.log("received token: "+info.token);
    }
    var url = "http://pelars.sssup.it:8080/pelars/multimedia?token="+token;
    //add all images to files
    var d = data.data;
    //var pelarsdata = [];
    //console.log(d);
    for(var attributename in d){
      var nd = d[attributename];
      for(var elem in nd){
        var nnd = nd[elem];
        for(var e in nnd){
          if (e == "type" && nnd[e]== "image"){
            var file = appRoot+"/public"+nnd["value"];

            fs.readFile(file, function(err, original_data){
              base64Image = original_data.toString('base64');
              var formData = {
                id: pelarsid,
                type: "image",
                data: base64Image,
                mimetype: "jpeg"
              };
              console.log(formData);
              console.log(url);

              request({
                  url: url,
                  method: 'PUT',
                  json: formData},
                function optionalCallback(err, httpResponse, body) {
                  if (err) {
                    return console.error('upload failed:', err);
                  }
                  console.log('Upload successful!  Server responded with:', body);
                });
            });
          } else if (e == "type" && nnd[e]== "textarea"){
            var b = new Buffer(nnd["value"]);
            var s = b.toString('base64');
            var bb = new Buffer('s', 'base64');
            var ss = b.toString();
            var formData = {
              id: pelarsid,
              type: "text",
              data: s,
              mimetype: "plain"
            };
            console.log(formData);
            request({
                url: url,
                method: 'PUT',
                json: formData},
              function optionalCallback(err, httpResponse, body) {
                if (err) {
                  return console.error('upload failed:', err);
                }
                console.log('Server responded with:', body);
              });
          }
        }
      }
    }
  });
}


var letsgo = require('../pelars');

/*
 * GET authoring.
 */
exports.index = function(req, res){
    var sId = req.params.sid, //get the id of the selected scenario
        Scenario = letsgo.getScenario();

    Scenario.findById(sId, function(err, scenario){
        res.render('authoring', {
            user: req.user,
            scenario: scenario
        });
    });

    // console.log(sName);
};

/*
 * GET scenarios.
 */
exports.scenarios = function(req, res){
    var Scenario = letsgo.getScenario();

    //find all scenarios created by the current user
    Scenario.find({ user: req.user._id })
            .sort({'name': 1})
            .exec(function(err, scenarios) { 
                //find all scenarios not created by the current user
                Scenario.find({ user: { $ne: req.user._id } })
                        .sort({'name': 1})
                        .populate('user', 'username') //get the username of the user who created the scenario
                        .exec(function(err, otherScenarios){ //execute query
                            res.render('scenarios', {
                                user:           req.user,
                                scenarios:      scenarios,
                                otherScenarios: otherScenarios,
                                baseUrl:        req.protocol + '://' + req.get('host') + '/mobile'
                            });
                        });
            });
    //res.end();
};

/*
 * POST scenarios
 */
exports.save = function(req, res){
    var Scenario = letsgo.getScenario();

    function saveFailed() {
        console.log('save failed');
        res.end('Scenario saved failed');
    }

    var sId = req.body.id;

    //find the scenario by ID
    Scenario.findById(sId, function(err, scenario){
        //if the scenario does not exist yet or was created by another user, 
        //create a new scenario
        if (!scenario || scenario.user + "" !== req.user._id + "") {     
            req.body.user = req.user._id; //store id of user who created the scenario

            var s = new Scenario(req.body); //create a new scenario

            s.save(function(err, savedScenario) {
                if (err){
                    console.log(err);
                    return saveFailed();
                } else {
                    console.log('Scenario saved');
                    res.send({scenarioId: savedScenario._id});
                }
            });
            console.log("nix gefunden");
        } 
        //if the scenario exists already, update it
        else {
            if (req.body.title){
                scenario.title = req.body.title;
            }
            if (req.body.description){
                scenario.description = req.body.description;
            }
            if (req.body.screenData){
                scenario.screenData = req.body.screenData;
            }

            // update lastModified value
            scenario.lastModified = new Date();

            scenario.save(function(err) {
                if (err){
                    console.log(err);
                    return saveFailed();
                } else {
                    console.log('Scenario updated');
                    res.end('Scenario updated');
                }
            });
        }
    });

    // Scenario.findOne({title: req.body.title},function(err, scenario){
    //     if (err){console.log(err);}
    //     if (!scenario || scenario.user != req.user._id) { 
            
    //         req.body.user = req.user._id; //store id of user who created the scenario

    //         var s = new Scenario(req.body);

    //         s.save(function(err, savedScenario) {
    //             if (err){
    //                 console.log(err);
    //                 return saveFailed();
    //             } else {
    //                 console.log('Scenario saved');
    //             }
    //         });
    //         console.log("nix gefunden");
    //     } else {
    //         console.log(req.body.title);

    //         //console.log(scenario);
    //         //scenario.description = req.body.description;
    //         if (req.body.title){
    //             scenario.title = req.body.title;
    //         }
    //         if (req.body.description){
    //             scenario.description = req.body.description;
    //         }
    //         if (req.body.screenData){
    //             scenario.screenData = req.body.screenData;
    //         }
    //         scenario.save(function(err) {
    //             if (err){
    //                 console.log(err);
    //                 return saveFailed();
    //             } else {
    //                 console.log('Scenario updated');
    //                 res.end('Scenario updated');
    //                 //res.redirect('letsgo4web');
    //             }
    //         });
    //     }

    //     /*
    //     res.render('authoring', {
    //         scenario: scenario
    //     });
    //     */
    // });


    //console.log(req.body);
};
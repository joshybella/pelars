var letsgo = require('../pelars');

var MEDIA_PATH = 'public/files/';

/*
 * Call to get the JSON of all scenarios.
 */
exports.getall = function(req, res) {
  var scenarioFromDB = letsgo.getScenario();
  var sName = req.params.sname;

  scenarioFromDB.find(function (err, scenarios) {
    res.json(scenarios);
  });
};

/*
 * Call to get the JSON of a certain scenario (req.params.sname).
 */
exports.getscenario = function(req, res) {
  var scenarioFromDB = letsgo.getScenario();
  var sName = req.params.sid;

  scenarioFromDB.findById(sName, function(err, scenario){
    res.json(scenario);
  });
};

/*
 * POST to create a new scenario with req.body
 */
exports.newscenario = function(req, res) {
  var scenarioFromDB = letsgo.getScenario();

  var Scenario = letsgo.getScenario();

  function saveFailed() {
    console.log('save failed');
    res.end('Scenario saved failed');
  }

  Scenario.find({ title: "req.body.title" }, function(err, scenario){
    if (!scenario){
      var s = new Scenario(req.body); //create a new scenario
      s.save(function(err, savedScenario) {
        if (err){
          console.log(err);
          return saveFailed();
        } else {
          console.log('Scenario saved');
          res.send({scenarioId: savedScenario._id});
        }
      });
    }
    else{
      console.log(err);
      res.send(err);
    }
  });
};

/*
 * Updates a scenario (req.params.sid).
 */
exports.updatescenario = function(req, res) {
  var scenarioFromDB = letsgo.getScenario();
  var sId = req.params.sid;

  function updateFailed() {
    console.log('update failed');
    res.end('Data update failed');
  }

  scenarioFromDB.findByIdAndUpdate(sId, {screenData: req.body.screenData}, function(err, scenario){
    if (err) {
      console.log(err);
      return updateFailed();
    }

    res.send(scenario._id);
  });
};

/*
 * Call to get the JSON of a certain scenario (req.params.sname).
 */
exports.deletescenario = function(req, res) {
  var scenarioFromDB = letsgo.getScenario();
  var scenarioID = req.params.sid;

  function remFailed() {
    console.log('remove failed');
    res.end('Scenario removal failed');
  }

  scenarioFromDB.findById(scenarioID, function(err, scenario){
    if (scenario){
      scenario.remove(function(err, removedScenario) {
        if (err){
          console.log(err);
          return remFailed();
        } else {
          console.log('Scenario removed');
          res.send({scenarioId: removedScenario._id});
        }
      });
    }
    else{
      console.log(err);
      res.send(err);
    }
  });
};

/*
 * Call to get the JSON of all saved data.
 */
exports.getalldata = function(req, res) {
  var collectedDataFromDB = letsgo.getCollectedData();
  var sID = req.params.sid;

  collectedDataFromDB.find(function(err, coldata){
    res.json(coldata);
  });
};

/*
 * Call to get the JSON of all saved data from a certain scenario (req.params.sid).
 */
exports.getdata = function(req, res) {
  var collectedDataFromDB = letsgo.getCollectedData();
  var dID = req.params.did;

  collectedDataFromDB.findById(dID, function(err, coldata){
    res.json(coldata);
  });
};

/*
 * Call to get the JSON of all saved data from a certain scenario (req.params.sid).
 */
exports.getscenariodata = function(req, res) {
  var collectedDataFromDB = letsgo.getCollectedData();
  var sID = req.params.sid;

  collectedDataFromDB.find({'scenarioId':sID}, function(err, coldata){
    res.json(coldata);
  });
};

/**
 * Call to get the JSON of all saved data by a certain user (req.params.uid)
 */
exports.getuserdata = function(req, res) {
  var collectedDataFromDB = letsgo.getCollectedData();
  var uID = req.params.uid;

  collectedDataFromDB.find({'_id':uID}, function(err, udata){
    res.json(udata);
  });
};

/**
 * Call to get the JSON of all saved data by a certain user (req.params.uid)
 */
exports.newdata = function(req, res) {
  var collectedDataFromDB = letsgo.getCollectedData();
  var sID = req.params.sid;

  var data = {
    groupname: req.body.groupname,
    scenarioId : sID,
    data: req.body.data
  }

  data.data = updateMediaValue(data.data);

  function saveFailed() {
    console.log('save failed');
    res.end('Scenario saved failed');
  }

  var d = new collectedDataFromDB(data); //create a new scenario

  d.save(function(err, savedData) {
    if (err){
      console.log(err);
      return saveFailed();
    } else {

      console.log('Data saved');
      res.send({scenarioId: savedData._id});
    }
  });
};

/**
 * Call to get the JSON of all saved data by a certain user (req.params.uid)
 */
exports.updatedata = function(req, res) {
  var collectedDataFromDB = letsgo.getCollectedData();
  var dID = req.params.did;

  var data = updateMediaValue(req.body.data);

  function updateFailed() {
    console.log('update failed');
    res.end('Data update failed');
  }

  collectedDataFromDB.findByIdAndUpdate(dID, { $set: { data: data }}, function (err, coldata) {
    if (err) {
      console.log(err);
      return updateFailed();
    }
    res.send(coldata._id);
  });
};

/**
 * delete a certain dataset where req.params.did is the ID of the dataset
 */
exports.deletedata = function(req, res) {
  var collectedDataFromDB = letsgo.getCollectedData();
  var dID = req.params.did;

  function remFailed() {
    console.log('remove failed');
    res.end('Data removal failed');
  }

  collectedDataFromDB.findById(dID, function(err, data){
    if (data){
      console.log(data);

      data.remove(function(err, removedData) {
        if (err){
          console.log(err);
          return remFailed();
        } else {
          console.log('Scenario removed');
          res.send({id: removedData._id});
        }
      });
    }
    else{
      console.log(err);
      res.send(err);
    }
  });
};

/*
 * Call to get the JSON of all saved media files.
 */
exports.getallmedia = function(req, res) {
  var collectedDataFromDB = letsgo.getCollectedData();
  var sName = req.params.sname;
  var allmedia = [];
  var temparray = [];

  collectedDataFromDB.find(function (err, mediadata) {
    for (var i=0; i<mediadata.length; i++) {
      console.log("data:  "+mediadata[i].data);
      if (mediadata[i].data == "image"){
        //console.log(mediadata[i].data);
        allmedia.push(mediadata[i].data);
      }
    }
    //console.log(mediadata.length);
    res.json(allmedia);
  });
};

/*
 * Call to get the JSON of a certain saved media file.
 */
exports.getmedia = function(req, res) {
  var scenarioFromDB = letsgo.getScenario();
  var sName = req.params.sname;

  scenarioFromDB.find(function (err, scenarios) {
    res.json(scenarios);
  });
};

/**
 * Hide a scenario
 */
exports.hide = function(req, res) {
  var scenarioFromDB = letsgo.getScenario();
  var sID = req.params.sid;

  scenarioFromDB.findById(sID, function(err, scenario){
    if (scenario){
      scenario.hidden = true;
      scenario.save(function(err){
        if (err){
          console.error(err);
          res.send(err);
        } else {
          console.log("changed the status of the scenario '"+sID+"' to hidden")
          res.send("hide");
        }
      })
    }
    else{
      console.error(err);
      res.send(err);
    }
  });
};

/**
 * Unhide a scenario
 */
exports.unhide = function(req, res) {
  var scenarioFromDB = letsgo.getScenario();
  var sID = req.params.sid;

  scenarioFromDB.findById(sID, function(err, scenario){
    if (scenario){
      scenario.hidden = false;
      scenario.save(function(err){
        if (err){
          console.error(err);
          res.send(err);
        } else {
          console.log("changed the status of the scenario '"+sID+"' to unhidden")
          res.send("unhide");
        }
      })
    }
    else{
      console.error(err);
      res.send(err);
    }
  });
};

exports.pelars = function(req, res) {
  console.log(req.files);
  res.send("yoyoyo");
};




//convert base64 string into image files
function updateMediaValue(data) {
  for (screen in data) { 
    for (var i = 0; i < data[screen].length; i++) {
      if (data[screen][i].type === 'image') {
        var path = newFile(data[screen][i].value);
        if (path.indexOf('public/') === 0) {
            path = path.substring(6);
        }
        data[screen][i].value = path;
      }
    }
  }

  return data;
}
function newFile(base64string) {
  var filename = (new Date()).getTime() + '' + Math.floor((Math.random() * 1000) + 1);
  var format = 'png'; //type === 'image' ? 'png' : type === 'video' ? 'mp4' : type === 'audio' ? '.mp3' : '';
  var path = MEDIA_PATH + '' + filename + '.' + format;
 
  require('fs').writeFile(path, base64string, 'base64', function(err) {
    console.log(err);
    return null;
  });
  return path;
}
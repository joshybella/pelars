angular.module('mobile', ['ionic', 'mobile.controllers', 'mobile.services'])

  .run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
    });
  })

  .config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
    //enable native scrolling
    $ionicConfigProvider.scrolling.jsScrolling(false);

    $stateProvider

      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: '/ionic/templates/menu.html',
        controller: 'AppCtrl',
        cache: false
      })
      .state('app.scenarios', {
        url: '/scenarios',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: '/ionic/templates/scenarios.html',
            controller: 'ScenariosCtrl'
          }
        }
      })
      .state('app.scenario', {
        url: '/scenarios/:scenarioId/:activity',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: '/ionic/templates/scenario.html',
            controller: 'ScenarioCtrl'
          }
        }
      })
      .state('app.activity', {
        url: '/activity/:scenarioId',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: '/ionic/templates/activity.html',
            controller: 'ActivityCtrl'
          }
        }
      })
    ;
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/scenarios');
  });

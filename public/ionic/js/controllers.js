angular.module('mobile.controllers', [])

  .controller('AppCtrl', function($scope, $ionicModal, $timeout, pelarsFactory) {
    // Form data for the login modal
    $scope.loginData = {};
    $scope.pelars = {};

    //localStorage.clear();

    //create and set a token
    pelarsFactory.createToken("janosch.zbick@lnu.se", "qwer1234").then(function(data){
      pelarsFactory.setToken(data.token);
    });

    /**
     * The login modal
     */
    $ionicModal.fromTemplateUrl('/ionic/templates/login.html', {
      scope: $scope
    }).then(function(modal) {
      $scope.modal = modal;
    });
    // Triggered in the login modal to close it
    $scope.closeLogin = function() {
      $scope.modal.hide();
    };
    // Open the login modal
    $scope.login = function() {
      $scope.modal.show();
    };
    // Perform the login action when the user submits the login form
    $scope.doLogin = function() {
      console.log('Doing login', $scope.loginData);
      pelarsFactory.createToken($scope.loginData.username, $scope.loginData.password).then(function(data){
        pelarsFactory.setToken(data.token);
      });
      //pelarsFactory.setID();
      $scope.modal.hide();
    };

    /**
     * The group modal
     */
    $ionicModal.fromTemplateUrl('/ionic/templates/group_modal.html', {
      scope: $scope
    }).then(function(modal) {
      if (localStorage.getItem('pelarsId')){
        pelarsFactory.setId(localStorage.getItem('pelarsId'));
        $scope.pelars.id = parseInt(localStorage.getItem('pelarsId'));
      }
      if (localStorage.getItem('groupname')){
        $scope.pelars.groupname = localStorage.getItem('groupname');
      }
      $scope.groupmodal = modal;
    });
    // Open the group modal
    $scope.showGroupModal = function() {
      $scope.groupmodal.show();
    };
    //closing the group modal
    $scope.closeGroup = function() {
      $scope.groupmodal.hide();
    };
    //perform the groupname change
    $scope.changeGroupname = function() {
      localStorage.setItem("groupname", $scope.pelars.groupname);
      localStorage.setItem("pelarsId", $scope.pelars.id);
      pelarsFactory.setId($scope.pelars.id);
      $scope.groupmodal.hide();
    };
  })

  .controller('ScenariosCtrl', function($scope, mlearn4webFactory) {
    $scope.scenarios = [];

    mlearn4webFactory.getScenarios().then(function (data) {
      data.forEach(function (item) {
        $scope.scenarios.push(item);
      })
    }, function () {
      console.error("error");
    });
    //console.log(mlearn4webFactory.getScenarios());
  })
  .controller('ActivityCtrl', function($scope, $stateParams, $ionicHistory, mlearn4webFactory, $ionicPopup) {
    $scope.scenario = {};
    $scope.data = {};
    $scope.badges = {
      plan: [],
      document: [],
      reflect: []
    };

    console.log(localStorage.getItem("groupname"));

    //show the altert if no group is defined and then go back
    $scope.showAlert = function() {
      $ionicPopup.alert({
        title: 'Groupname',
        content: 'Please provide a groupname and a PELARS ID'
      }).then(function() {
        $ionicHistory.goBack();
      });
    };
    //check if a group is defined
    if (!(localStorage.getItem("groupname")) || (localStorage.getItem("groupname") == "undefined")){
      $scope.showAlert();
    } else {
      //get the collected data from the scenario
      mlearn4webFactory.getScenarioData($stateParams.scenarioId)
        .then(function (res) {
          res.forEach(function (item) {
            if (localStorage.getItem("groupname") == item.groupname){
              switch (item.screen){
                case "0":
                  $scope.badges.plan.push(item);
                  break;
                case "1":
                  $scope.badges.document.push(item);
                  break;
                case "2":
                  $scope.badges.reflect.push(item);
                  break;
              }
              //console.log(item.screen);
            }
          });
          console.log($scope.badges);
        });
    }




    //get the data of the scenario
    mlearn4webFactory.getScenario($stateParams.scenarioId)
      .then(function (res) {
        $scope.scenario = res;
      }, function (err) {
        console.error(err);
      });
  })

  .controller('ScenarioCtrl', function($scope, $state, $stateParams, $ionicHistory, $ionicLoading, mlearn4webFactory, pelarsFactory) {
    $scope.scenario = {};
    $scope.data = {};
    $scope.choicedata = {};
    $scope.files = [];
    $scope.act = $stateParams.activity;
    $scope.matrix = {
      hheaders: ["Shared Understanding","Taking Actions","Team Organization"],
      vheaders: [],
      choices: []
    };
    $scope.obs = false;
    $scope.rate = false;
    var act;

    //check which PELARS activity
    switch ($stateParams.activity){
      case "0":
        act = "plan";
        break;
      case "1":
        act = "document";
        break;
      case "2":
        act = "reflect";
        break;
      case "3":
        act = "rate";
        $scope.rate = true;
        break;
    }

    //get the data of the scenario
    mlearn4webFactory.getScenario($stateParams.scenarioId).then(function (res) {
      $scope.scenario = res;
      $scope.scenario.activity = act;

      if (res.type == "observe"){
        $scope.obs = true;
        pelarsFactory.setCreator("observer");
        var str = "obs_";
        pelarsFactory.setPhase(str+=act);
        //send phase to pelars
        pelarsFactory.sendPhase(str);
        //set the matrix data
        for (var item in res.screenData[$stateParams.activity]){
          var current = res.screenData[$stateParams.activity][item];
          current.forEach(function(ele){
            if (ele.type == "choice"){
              $scope.matrix.vheaders.push(ele.title);
              //save the choices
              var tmp = [];
              ele.content.forEach(function (e) {
                tmp.push(e["[answer]"]);
              });
              $scope.matrix.choices.push(tmp);
            } else if (ele.type == "rate"){

            }
          })
        }
        console.log(res);
      } else {
        pelarsFactory.setCreator("student");
        pelarsFactory.setPhase(act);
      }
    }, function (err) {
      console.error(err);
    });

    //onchange listener for input fields
    $scope.change = function (e) {
      if (e.type == "choice"){
        var tmp = e.value.split(':');
        if (!$scope.data.choice){
          $scope.data.choice = {};
          $scope.data["choice"][tmp[1]] = tmp[0]+':'+tmp[2];
        } else {
          $scope.data["choice"][tmp[1]] = tmp[0]+':'+tmp[2];
        }
        pelarsFactory.sendData(e.type, tmp[0]+':'+tmp[2]);
      } else {
        pelarsFactory.sendData(e.type, e.value);
      }
      console.log($scope.data);
    };

    //onchange listener for file fields
    $scope.uploadFile = function (file, type) {
      var tmp = file.id.split("_");
      var index = tmp[1].match(/\d+/)[0];

      if (file.files){
        $scope.files.push(file.files[0]);
        mlearn4webFactory.saveFiles(file.files, type)
          .then(function (res) {
            $ionicLoading.hide();
            //add the files to the data model
            if (!$scope.data.files){
              $scope.data.files = {}
            }
            $scope.data.files[index] = {
              type: type,
              path: res.data
            };
          });
        //convert to base64
        var reader = new FileReader();
        reader.onloadend = function (e) {
          //console.log(e.target.result);
          var img64 = e.target.result;
          var str = img64.split(",")[1];
          pelarsFactory.sendData(type, str);
        };
        reader.readAsDataURL(file.files[0]);
      }
    };

    //aubmit to mLearn4web
    $scope.submit = function () {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      mlearn4webFactory.saveData($scope.data, $scope.scenario._id, $stateParams.activity, pelarsFactory.getId())
        .then(function (res) {
          $ionicLoading.hide();
          $ionicHistory.goBack();
        });

    }
  })
;

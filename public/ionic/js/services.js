angular.module('mobile.services', [])
  .factory('mlearn4webFactory', ['$http', '$ionicLoading', function($http, $ionicLoading) {

    var urlBase = 'http://mlearn4web.eu:8070/mlearn4web';
    var mlearn4webFactory = {};

    mlearn4webFactory.getScenarios = function () {
      return $http.get(urlBase+"/getall").then(function (res) {
        return res.data;
      });
    };

    mlearn4webFactory.getScenario = function (id) {
      return $http.get(urlBase + '/get/' + id).then(function (res) {
        return res.data;
      });
    };

    mlearn4webFactory.getScenarioData = function (scenarioId) {
      console.log(scenarioId);
      return $http.get(urlBase + '/getscenariodata/' + scenarioId).then(function (res) {
        return res.data;
      });
    };

    //save files to mLearn4web
    mlearn4webFactory.saveFiles = function (files, type) {
      $ionicLoading.show({
        template: '<p>processing file...</p><ion-spinner></ion-spinner>'
      });
      return $http({
        headers: {'Content-Type':undefined},
        url: 'http://www.mlearn4web.eu:8070/mobile/savedata',
        method: "POST",
        transformRequest: function (data) {
          var formData = new FormData();
          formData.append("model", angular.toJson(data.model));
          formData.append("file", files[0]);
          /*for (var i = 0; i < data.files; i++) {
            //add each file to the form data and iteratively name them
            formData.append("file" + i, data.files[i]);
          }*/
          return formData;
        },
        data: {files: files, model: {origin: 'media', type: type}}
      }).success(function(res) {
        return res;
      });
    };

    mlearn4webFactory.saveData = function (data, scenarioId, activity, pelarsId) {
      var postData = {
        data: data,
        origin: "pelars",
        scenario : scenarioId,
        activity : activity,
        groupname: localStorage.getItem("groupname"),
        pelarsId: pelarsId
      };


      //working for json
      return $http({
        headers: {'Content-Type': 'application/json'},
        url: 'http://www.mlearn4web.eu:8070/mobile/savedata',
        method: "POST",
        data: postData
      }).success(function(res) {
        return res.data;
      });

    };

    return mlearn4webFactory;
  }])

  .factory('pelarsFactory', ['$http', function($http) {
    var urlBase = 'http://pelars.sssup.it/pelars';
    var pelarsFactory = {};
    var pelarsData = {};

    if (localStorage.getItem('pelarsId')){
      pelarsData.id = localStorage.getItem('pelarsId');
    }

    function bas64(data) {
      var s = btoa(data);
      return s;
    }

    /**
     * PELARS token
     */
    //create a token
    pelarsFactory.createToken = function (user, pwd) {
      return $http.post(urlBase+'/password?user='+user+'&pwd='+pwd).then(function (res) {
        return res.data;
      });
    };
    //set the token in the model
    pelarsFactory.setToken = function (token) {
      pelarsData.token = token;
    };
    //get the token
    pelarsFactory.getToken = function () {
      return pelarsData.token;
    };
    /**
     * PELARS ID
     */
    //set the id
    pelarsFactory.setId = function (id) {
      pelarsData.id = id;
    };
    //get the id
    pelarsFactory.getId = function () {
      return pelarsData.id;
    };
    /**
     * PELARS phase
     */
    pelarsFactory.sendPhase = function (phase) {
      var data = {
        phase: phase,
        time: Date.now()
      };

      return $http.put(urlBase+'/phase/'+pelarsData.id+'?token='+pelarsData.token, data)
        .then(function (res) {
          return res.data;
        })
    };
    //set the phase
    pelarsFactory.setPhase = function (phase) {
      pelarsData.phase = phase;
    };
    //get the phase
    pelarsData.getPhase = function () {
      return pelarsData.phase;
    };
    /**
     * PELARS creator
     */
    //set the creator
    pelarsFactory.setCreator = function (creator) {
      pelarsData.creator = creator;
    };
    //get the creator
    pelarsData.getCreator = function () {
      return pelarsData.creator;
    };

    //get data from timeserver


    pelarsFactory.sendData = function (type, data) {
      //set the json data
      var d = {
        id: pelarsData.id,
        view: "mobile",
        creator: pelarsData.creator,
        trigger: "manual",
        phase: pelarsData.phase,
        group: localStorage.getItem("groupname"),
        time: Date.now()
      };
      //check what type and add data
      switch (type){
        case "text":
        case "numeric":
        case "choice":
          d.type = "text";
          d.mimetype = "plain";
          d.data = bas64(data);
          break;
        case "image":
          d.type = "image";
          d.mimetype = "jpeg";
          d.data = data;
          break;
        case "video":
          d.type = "video";
          d.mimetype = "mp4";
          d.data = data;
          break;
      }

      return $http.put(urlBase+'/multimedia?token='+pelarsData.token, d).then(function (res) {
        return res.data;
      });
    };
    
    return pelarsFactory;
  }])
;
var authoringApp = angular.module('authoringApp', ['ui.sortable']);

authoringApp.controller('ScreenListCtrl', function ($scope) {
  $scope.screens = ["Screen 1","Screen 2","Screen 3"];

  $scope.screenClick = function(screen){
    console.log(screen);
  };

  $scope.dragControlListeners = {
    accept: function (sourceItemHandleScope, destSortableScope){
      return boolean
    },//override to determine drag is allowed or not. default is true.
    itemMoved: function (event) {
      //Do what you want},
    },
    orderChanged: function(event) {
      //Do what you want}
    },
    containment: '#board'//optional param.
    };
});

authoringApp.controller('ElementListCtrl', function ($scope) {
  $scope.elements = [
    {type: "Instruction", icon: "glyphicon-text-height"},
    {type: "Text", icon: "glyphicon-font"},
    {type: "Image", icon: "glyphicon-camera"},
    {type: "Video", icon: "glyphicon-facetime-video"},
    {type: "Sound", icon: "glyphicon-music"},
    {type: "Choice", icon: "glyphicon-th-list"},
    {type: "Numerical", icon: "glyphicon-plus-sign"},
    {type: "Date", icon: "glyphicon-calendar"},
    {type: "Location", icon: "glyphicon-map-marker"}
  ];

  $scope.screenClick = function(screen){
    console.log(screen.element[0]);
  };

  $scope.dragControlListeners = {
    accept: function (sourceItemHandleScope, destSortableScope){
      return boolean
    },//override to determine drag is allowed or not. default is true.
    itemMoved: function (event) {
      //Do what you want},
    },
    orderChanged: function(event) {
      //Do what you want}
    },
    containment: '#board'//optional param.
  };
});
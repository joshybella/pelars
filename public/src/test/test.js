var activityApp = angular.module('activityApp', ['ui.bootstrap', 'smart-table']);

activityApp.controller('ActivityCtrl', function ($rootScope, $scope, getUser, getActivities, getLoc) {

  getActivities.then(function (data) {
    var user = document.getElementById('body').getAttribute("uID");

    $rootScope.allActivities = [];
    $rootScope.ownActivities = [];

    data.forEach(function(element){
      if (element.user == user){
        $rootScope.ownActivities.push(element);
      } else {
        element.location = d.results[0].formatted_address;
        getUser.fn(element.user).then(function(dat){
          element.username = dat.username;
          $rootScope.allActivities.push(element);
        });
      }
    });

    //console.log(own_act);
  });

  $scope.displayedOwnCollection = [].concat($rootScope.ownActivities);
  $scope.displayedAllCollection = [].concat($rootScope.allActivities);

});

activityApp.factory('getActivities', function ($http, $q){
  var defferer = $q.defer();

  $http.get('/mlearn4web/getall').success(function (data){
    defferer.resolve(data);
  });

  return defferer.promise;
});

activityApp.factory('getLoc', function ($http, $q) {
  return {
    fn: function(latlng, callback){
      //console.log(latlng);
      var defferer = $q.defer();
      $http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+latlng).success(function (data){
        //console.log(data.results[0].formatted_address);
        defferer.resolve(data);
      });
      return defferer.promise;
    }
  };
});

activityApp.factory('getUser', function ($http, $q) {
  return {
    fn: function(user, callback){
      //console.log(latlng);
      var defferer = $q.defer();
      $http.get('/api/getUserById/'+user).success(function (data){
        //console.log(data.results[0].formatted_address);
        defferer.resolve(data);
      });
      return defferer.promise;
    }
  };
});


activityApp.controller('DelModalCtrl', function ($scope, $modal, $log) {

  $scope.items = ['item1', 'item2', 'item3'];

  $scope.open = function (aID) {
    var modalInstance = $modal.open({
      templateUrl: 'delModalContent.html',
      controller: 'DelModalInstanceCtrl',
      resolve: {
        aID: function(){
          return aID;
        }
      }
    });
  };
});

activityApp.controller('DelModalInstanceCtrl', function ($rootScope, $scope, $modalInstance, $http, aID) {
  $scope.ok = function () {
    $http.get('/authoring/delete/'+aID).
      success(function(data, status, headers, config) {
        $rootScope.ownActivities.forEach(function(element, index){
          if (element._id == aID){
            $rootScope.ownActivities.splice(index,1);
          }
        })
      }).
      error(function(data, status, headers, config) {
        console.log(data);
      });
    console.log(aID);
    $modalInstance.close();
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});
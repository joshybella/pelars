$(window).load(function(){

    var element1 = '';
    var textField1 = '<input type="text" disabled="disabled"/>';
    var textArea1 = '<textarea disabled="disabled"></textarea>';
    var image1 = '<button disabled="disabled">Take a Snapshot</button>';
    var video1 = '<button disabled="disabled">Make a Video</button>';
    var sound1 = '<button disabled="disabled">Start Recording</button>';
    var choice1 = '<div class="choices"><input class="1" type="radio" name="choice" value="option1" disabled="disabled"><input class="label" type="text" placeholder="Option 1"/><br><input class="2" type="radio" name="choice" value="option2" disabled="disabled"><input class="label" type="text" placeholder="Option 2"/><br>';
    var numerical1 = '<input type="number" disabled="disabled"/>';
    var date1 = '<input type="date" disabled="disabled"/>';
    var location1 = '<button disabled="disabled">Add Current Location</button>';

    var QueryString = function () {
        // This function is anonymous, is executed immediately and
        // the return value is assigned to QueryString!
        var query_string = {};
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i=0;i<vars.length;i++) {
            var pair = vars[i].split("=");
            // If first entry with this name
            if (typeof query_string[pair[0]] === "undefined") {
                query_string[pair[0]] = pair[1];
                // If second entry with this name
            } else if (typeof query_string[pair[0]] === "string") {
                var arr = [ query_string[pair[0]], pair[1] ];
                query_string[pair[0]] = arr;
                // If third or later entry with this name
            } else {
                query_string[pair[0]].push(pair[1]);
            }
        }
        return query_string;
    } ();

    $(".scenario-element textarea.label").autosize();

    function loadScenario(id){

        //drop functionality
        function droppable1(screenID){
            //allow dropping new elements into the scenario
            $(screenID).droppable({
                drop: function( event, ui ) {
                    var elementName = ui.draggable[0].classList[0];
                    console.log('dropping, element name: '+elementName);
                    if(!$(ui.draggable[0]).hasClass('dropped')){

                        switch (elementName){
                            case 'textfield':
                                element1 = textField1;
                                break;
                            case 'textarea':
                                element1 = textArea1;
                                break;
                            case 'image':
                                element1 = image1;
                                break;
                            case 'video':
                                element1 = video1;
                                break;
                            case 'sound':
                                element1 = sound1;
                                break;
                            case 'choice':
                                element1 = choice1;
                                break;
                            case 'numerical':
                                element1 = numerical1;
                                break;
                            case 'date':
                                element1 = date1;
                                break;
                            case 'location':
                                element1 = location1;
                                break;
                            default:
                                break;
                        }
                        console.log('calling parseElement,  element1=' + element1);

                        if (required) {
                            $(screenID).append('<div class="scenario-element dropped '+ currentElement.type +'"><textarea class="label" placeholder="Label" value=""/></br><div class="content required">' + element1 + '</div><div class="required-switch"><input type="checkbox" checked="checked" name="required" value="true"><span>Required</span></div></div>').find('textarea.label').trigger('autosize.resize');
                        } else {
                            $(screenID).append('<div class="scenario-element dropped '+ currentElement.type +'"><textarea class="label" placeholder="Label" value=""/></br><div class="content">' + element1 + '</div><div class="required-switch"><input type="checkbox" name="required" value="true"><span>Required</span></div></div>').find('textarea.label').trigger('autosize.resize');
                        }
                    }
                }
            });
        };

        //make it possible to rearrange elements
        function sortable1(screenID) {
            $(screenID).sortable({
                connectWith: ".screen",
                cursor: "move",
                start: function(event, ui) {
                    is_dragging = true;
                },
                stop: function(event, ui) {
                    is_dragging = false;
                }
            }).disableSelection();
        };

        // $('#save-scenario').html('<div class="icon icon-upload-cloud"></div>Update').addClass('update');

        $.ajax({
            type: "GET",
            url: '/getscenario?scenario_id=' + id,
            success: function(data){
                console.log('data'+data);
                var scenario = JSON.parse(data);

                $('#scenario-title').val(scenario.title);
                $('#scenario-type').val(scenario.organizationType);
                $('#scenario-start').val(scenario.startDate);
                $('#scenario-end').val(scenario.endDate);
                $('#scenario-description').val(scenario.description);

                //TODO: parse screens here
                var screenData =  JSON.parse(scenario.screenData);

                for(var i = 0; i < screenData.length; i++) {

                    console.log('screenData');
                    console.log(screenData);

                    var screenNumber = i + 1;
                    $("#screen-nav").append('<div id="nav'+screenNumber+'" class="dropped nav-item"><span class="icon-mobile-1"></span><a href="#screen'+ screenNumber +'">Screen '+ screenNumber +'</a><span class="icon-cancel"></span></div>');
                    $('#screens').append('<div id="screen'+screenNumber+'" class="screen block"></div>');
                    $("#nav" + screenNumber).droppable({
                        over: function( event, ui ) {
                            var targetId = $(event.target).attr('id').replace('nav', '');
                            $("a[href=#screen"+ targetId + "]").trigger('click');
                        }
                    });
                    var targetScreen = '#screen'+screenNumber;
                    droppable1(targetScreen);
                    sortable1(targetScreen);

                    var screen = screenData[i];

                    console.log('screen, screen.length='+screen.length);
                    console.log(screen);


                    for (var j = 0; j < screen.screenElements.length; j++) {


                        var currentElement = screen.screenElements[j];
                        console.log('element #'+j+' is '+currentElement);
                        console.log(currentElement);

                        var required = false;
                        if (currentElement.required) {
                            required = true;
                        }

                        switch (currentElement.type){
                            case 'textfield':
                                element1 = textField1;
                                break;
                            case 'textarea':
                                element1 = textArea1;
                                break;
                            case 'image':
                                element1 = image1;
                                break;
                            case 'video':
                                element1 = video1;
                                break;
                            case 'sound':
                                element1 = sound1;
                                break;
                            case 'choice':
                                element1 = choice1;
                                break;
                            case 'numerical':
                                element1 = numerical1;
                                break;
                            case 'date':
                                element1 = date1;
                                break;
                            case 'location':
                                element1 = location1;
                                break;
                            default:
                                break;
                        }

                        console.log('calling parseElement,  element1=' + element1);

                        if (required) {
                            $(targetScreen).append('<div class="scenario-element dropped '+ currentElement.type +'"><textarea class="label" placeholder="Label" value="'+currentElement.title+'"/></br><div class="content required">' + element1 + '</div><div class="required-switch"><input type="checkbox" checked="checked" name="required" value="true"><span>Required</span></div></div>');
                        } else {
                            $(targetScreen).append('<div class="scenario-element dropped '+ currentElement.type +'"><textarea class="label" placeholder="Label" value="'+currentElement.title+'"/></br><div class="content">' + element1 + '</div><div class="required-switch"><input type="checkbox" name="required" value="true"><span>Required</span></div></div>');
                        }

                    }
                }


            }
        });
    };

    if (QueryString.id) {
        $('#scenario-head').addClass(QueryString.id);
        loadScenario(QueryString.id);
    }
    else {
        console.log('new');
    }

});
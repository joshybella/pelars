$( document ).ready(function() {
	var userID = $("#user").attr("uid");

	$("#bt_new").click(function(){
	ga('send', 'event', 'user_action', 'new_scenario', userID);
	});

	$(".list-group-item").click(function(){
	ga('send', 'event', 'user_action', 'edit_scenario', userID);
	});

	$(document).on('click', '.remove-scenario', function(event) {
		event.preventDefault();
	});

	$(document).on('click', '#remove-scenario-button', function() {
		var id = $(this).attr('data-id');
		deleteScenario(id);
		// $('#remove-scenario-dialog').modal('hide');
	});

	$('#remove-scenario-dialog').on('show.bs.modal', function (event) {
	  var button = $(event.relatedTarget); // Button that triggered the modal
	  console.log(button);
	  var id = $(button).parents('tr').attr('data-id');
	  var title = $(button).parents('tr').attr('data-title');

	  console.log("ID: " + id);
	  console.log("TITLE: " + title);

	  //var recipient = button.data('whatever') // Extract info from data-* attributes
	  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	  var modal = $(this);
	  modal.find('#remove-scenario-title').text(title);
	  modal.find('#remove-scenario-button').attr('data-id', id);
	});


	if ($('#scenarios table tbody tr').length) {
		$('#scenarios table').tablesorter({
			sortList: [[0,0]],
			headers: { 
	            3: { sorter: false },
	            4: { sorter: false },
	            5: { sorter: false }
	        } 
		}); 
	}
	if ($('#otherScenarios table tbody tr').length) {
		$('#otherScenarios table').tablesorter({
			sortList: [[2,0]]
			// headers: { 
	  //           4: { sorter: false }
	  //       } 
		});
	} 

	$('#qrCodeModal').on('show.bs.modal', function (event) {
	  var button = $(event.relatedTarget); // Button that triggered the modal
	  var url = button.data('url'); // Extract info from data-* attributes
	  var title = button.data('title');
	  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	  var modal = $(this);
	  modal.find('.modal-title').text('QR Code for ' + title);
	  modal.find('.modal-body div.qrcode').html('');
	  modal.find('.modal-body div.qrcode').qrcode(url);

	  $('#qrCodeModal .print').click(function() {
	  	$('.print-content .title').html(modal.find('.modal-title').html());
	  	$('.print-content div.qrcode').html('');
	  	$('.print-content div.qrcode').qrcode(url);
	  	var title = $('title').html();
		$('title').html('');
	  	window.print();
	  	$('title').html(title);
	  	$('.print-content .title').html('');
	  	$('.print-content div.qrcode').html('QR code was not loaded properly... Please try again!');
	  });
	});

	$(".hide-scenario").click(function(){
		var id = $(this).attr("sid");
		$.get('/mlearn4web/hide/' + id, function() {
			location.reload();
		});
	});

	$(".unhide-scenario").click(function(){
		var id = $(this).attr("sid");
		$.get('/mlearn4web/unhide/' + id, function() {
			location.reload();
		});
	});
});

function deleteScenario(id) {
    $.get('/mlearn4web/deletescenario/' + id, function() {
    	location.reload();
    });
}
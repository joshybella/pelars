var currentScenario = "";

$(document).delegate("#mainscreen", "pageshow", function() {
  console.log("mainscreen");
  $(".btgoactlist").click(function(){
    currentScenario = this.id;
    //console.log(this.id);
    $.mobile.changePage("#actlist");
  });
});

$(document).delegate("#actlist", "pageshow", function() {
  //console.log(activities);
  $('#alist').empty();

  $.each(activities, function(index, obj){
    if (obj.scenarioId == currentScenario){
      $('#alist').append($('<li/>', {
        'data-icon': "false"
      }).append($('<a/>', {
        'href': '/mobile/activity/'+obj._id,
        'rel': 'external',
        'id': 'id',
        'scenario': currentScenario,
        'data-transition': 'slide',
        'text': obj.title
      })));
    }
  });

  //add the "New Activity" button
  $('#alist').append($('<li/>', {
    'data-icon': "plus",
    'data-theme': "b"
  }).append($('<a/>', {
    'href': '#popupDialog',
    'data-rel': 'popup',
    'data-position-to': 'window',
    'data-transition':'pop',
    'text': 'New Activity'
  })));

  $('#alist').listview('refresh');

  $("#okbtn").click(function(){
    //create a new activity
    var title = $("#title").val();
    $("#title").val("");
    $.post("/mobile/newActivity", {
        scenarioId: currentScenario,
        title: title
      },
      function (data) {
        console.log(data._id);
        window.location = "/mobile/activity/"+data._id;
      },
      "json"
    );
  });
});
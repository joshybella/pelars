$(function() {
	
	var numScreens = $("div[data-role='page']").length - 2;
  var token = "";
  var pelars = {};
  var d = {};
	var creator = "";

  var pathname = window.location.pathname;

  var temp = pathname.split("/");
  var phase = temp[temp.length-1];

  if (phase == "0"){
    if (s.type == "observe"){
      phase = "obs_plan";
			creator = "observer"
    } else {
      phase = "plan";
			creator = "student"
    }
  } else if (phase == "1"){
    if (s.type == "observe"){
      phase = "obs_document";
			creator = "observer"
    } else {
      phase = "document";
			creator = "student"
    }

  } else if (phase == "2"){
    if (s.type == "observe"){
      phase = "obs_reflect";
			creator = "observer"
    } else {
      phase = "reflect";
			creator = "student"
    }
  }  else if (phase == "3"){
		if (s.type == "observe"){
			phase = "obs_rate";
			creator = "observer"
		} else {
			phase = "rate";
			creator = "student"
		}
	}

	$(document).on("pagecreate","[data-role='page']", function() {
		var $this = $(this);
		var pageId = $this.attr('id');

		console.log('page created: ' + pageId);

		if (pageId == 'mainscreen') {
			if (numScreens > 0) {
			 	$this.on('swipeleft', function(event) {
			 		console.log('swipeleft');
			 		$.mobile.changePage('#screen1');
			 		event.preventDefault();
			 	});
			}
		} else if (pageId == 'submitscreen') {
			$this.on('swipeleft', function(event) {
		 		console.log('swiperight');
		 		$.mobile.changePage('#screen' + numScreens);
		 		event.preventDefault();
		 	});
		} else {
			var c = parseInt(pageId.substr(6,1));
			console.log('c: ' + c);

			$this.on('swiperight', function(event) {
				console.log('swiperight');
				if (c == 1) {
					$.mobile.changePage('#mainscreen');
				} else {
					$.mobile.changePage('#screen' + (c - 1));
				}

		 		event.preventDefault();
			});
		
			if (c < numScreens) {
				$this.on('swipeleft', function(event) {
					console.log('swipeleft');
				 	$.mobile.changePage('#screen' + (c + 1));
				 	event.preventDefault();
				});
			}

			if (c == numScreens) {
				$this.on('swipeleft', function(event) {
					console.log('swipeleft');
				 	$.mobile.changePage('#submitscreen');
				 	event.preventDefault();
				});
			}

			// ACTIVATE CAPTURE MEDIA (image, video, audio)
			$this.find('.captureimage, .capturevideo, .captureaudio').each(function() {
				//console.log('captureimage/video/audio');
				var id = $(this).attr('id');
				$('#' + id + 'link').click(function() { 
					$('#' + id).click(); 
				}); 
				$('#' + id).change(function() { 
					$('#' + id + 'textfield').val($('#' + id).val()); 

					//display image preview
					var input = this;
					if (input.files && input.files[0]) {
				        var reader = new FileReader();

				        reader.onload = function (e) {
				            $('#' + id + 'image').attr('src', e.target.result);
				            //$('#' + id + 'image').show();
				        };

				        reader.readAsDataURL(input.files[0]);
				    }
				});
			});

			// ACTIVATE GEOLOCATION
			$this.find('.capturegeolocation').each(function() {
				console.log('capturegeolocation');
				var id = $(this).attr('id');
				$('#' + id + 'link').click(function() { 
					if (navigator.geolocation) {
						navigator.geolocation.getCurrentPosition(showPosition);
					} else {
						console.log('Geolocation is not supported by this browser.');
					}

					function showPosition(position) {
						$('#' + id).val(position.coords.latitude + ';' + position.coords.longitude);
						$('#' + id + 'textfield').val(position.coords.latitude + '; ' + position.coords.longitude);

						drawMap(position.coords.latitude, position.coords.longitude);
					}

					function drawMap(lat, lng) {
						console.log("draw map id: " + id);
						var myLatlng = new google.maps.LatLng(lat, lng);
					  	var mapOptions = {
					    	zoom: 8,
					    	center: myLatlng
					  	};
					  	var map = new google.maps.Map(document.getElementById(id + 'map'), mapOptions);

					  	var marker = new google.maps.Marker({
					    	position: myLatlng,
					      	map: map
					  	});
					}
				});
			});

			// ACTIVATE DEVICE ORIENTATION
			$this.find('.captureorientation').each(function() {
				console.log('captureorientation');
				var id = $(this).attr('id');
				$('#' + id + 'link').click(function() {
					if (window.DeviceOrientationEvent) {
						window.addEventListener('deviceorientation', deviceOrientationHandler, false);
					} else {
						console.log('Device Orientation is not supported by this browser.');
					}

					function deviceOrientationHandler(eventData) {
						var tiltLR = eventData.gamma;
						var tiltFB = eventData.beta;
						var dir = eventData.alpha;
						$('#' + id).val(Math.round(dir) + ';' + Math.round(tiltFB) + ';' + Math.round(tiltLR));
						$('#' + id + 'textfield1').val('Tilt Left/Right: ' + Math.round(tiltLR));
						$('#' + id + 'textfield2').val('Tilt Front/Back: ' + Math.round(tiltFB));
						$('#' + id + 'textfield3').val('Direction: ' + Math.round(dir));
						window.removeEventListener('deviceorientation', deviceOrientationHandler);
					} 
				});
			});

			// ACTIVATE QR SCANNER
			$this.find('.captureqr').each(function() {
				console.log('captureqr');
				var id = $(this).attr('id');
				console.log('id: ' + id);
				var video = document.getElementById(id + 'video');
				var cam = camera(id + 'video', id + 'canvas');

				function readQR(qrcode) { // should overwrite the in camera.js already existing function
					if (qrcode == "Couldn't find enough finder patterns") {
	    				$('#' + id).val('');
	    				$('#' + id + 'textfield').val('');
					} else {
						$('#' + id).val(qrcode);
						$('#' + id + 'textfield').val(qrcode);
	   			   		video.pause();
	   			   		cam.stop();
	    				$('#' + id + 'video').css('visibility', 'hidden');
	    	    	    $('#' + id + 'link').css('visibility', 'visible');
	    			}
				}
				qrcode.callback = readQR;

				navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
				if (navigator.getUserMedia) {
			    	$('#' + id + 'link').click(function() {
						$('#' + id + 'video').css('visibility', 'visible');
			    	   	$('#' + id + 'link').css('visibility', 'hidden');
			    	   	navigator.getUserMedia({'audio': false, 'video': true }, function(stream) { 
			     	   		video.src = (window.URL && window.URL.createObjectURL(stream)) || stream;
			    	   	    video.play();
					        cam.start();
			   	   		}, function(error) {
			   	      		console.log(error);
			    	   	});
			    	});
				} else {
					console.log('QR-Code Reader is not supported by this browser.');
				}
			});
		}
	});


	$('form').on('submit', function() {
		var submit = true;
		$(this).find('input.required').each(function() {
			var $input = $(this); 
			if (!$input.val()) {
				submit = false;
				console.log("Value required: " + $input.attr('id'));

				// $input.parent('.ui-field-contain').addClass('invalid-input');

				if ($input.hasClass('hidden-input')) {
					$input.siblings().find('input').each(function() {
						var $siblingInput = $(this);
						if (! $siblingInput.hasClass('hidden-input')) {
							$siblingInput.closest('div').addClass('invalid-input');
						}
					})
				} else {
					$input.closest('div').addClass('invalid-input');
				}
			} else {
				if ($input.hasClass('hidden-input')) {
					$input.siblings().find('input').each(function() {
						var $siblingInput = $(this);
						if (! $siblingInput.hasClass('hidden-input')) {
							$siblingInput.closest('div').removeClass('invalid-input');
						}
					})
				} else {
					$input.closest('div').removeClass('invalid-input');
				}
			}
		});

		if (!submit) {
			$('#invalid-input-popup').popup('open', { positionTo: '#submit-btn' });
		}

		$("#floatingCirclesG").addClass("loadingAnimation");
		$("#floatingCirclesG").removeClass("loadingAnimationHidden");

		return submit;
	});

	$('form input,select').on("keyup keypress", function(e) {
	  var code = e.keyCode || e.which; 
	  if (code  == 13) {               
	    e.preventDefault();
	    return false;
	  }
	});

  $(document).delegate(".main", "pageshow", function() {
    pelars.login = $("#pelarslogin").val();
    pelars.password = $("#pelarspassword").val();
    pelars.id = $("#pelarsid").val();

    $.post('http://pelars.sssup.it/pelars/password?user='+pelars.login+'&pwd='+pelars.password, function( data ) {
      if (data.token){
        token = data.token;
				console.log("Token; "+token);
        if (~phase.indexOf("obs_") ){
          //send the phase to PELARS
          var u = "http://pelars.sssup.it/pelars/phase/"+pelars.id+"?token="+token;
          var fData = {
            "phase": phase,
            "time": Date.now()
          };

          /*$.put(u,JSON.stringify(fData),function(result){
            console.log(result);
          });*/

          $.ajax({
            type: "PUT",
            url: u,
            data: JSON.stringify(fData),
            success: function(){console.log("yeah")},
            dataType: "json",
            contentType : "application/json; charset=utf-8"
          });

        }
      } else {
        console.log("No token - wrong PELARS credentials");
      }
    }).fail( function(){
        alert("Failed to connect to PELARS, please check credentials and internet connection");
      }

    );
  });

  $(".change").change(function(){
    if ($(this).hasClass("text")){
      if (phase == "rate" || phase == "obs_rate"){
        console.log("desc");
        d.description = $(this).val();
        sendScoreToPlears($(this).val(), "description");
      } else {
        var base64 = b64EncodeUnicode($(this).val());
        sendToPelars(base64, "text");
      }
    } else if ($(this).hasClass("choice")){
      if (phase == "rate" || phase == "obs_rate"){
        console.log("rate");
        d.score = $(this).val();
        sendScoreToPlears($(this).val(), "score");
      } else{
        var base64 = b64EncodeUnicode($(this).val());
        sendToPelars(base64, "choice");
      }
    } else if ($(this).hasClass("image")){
      readImage(this, "image");
    } else if ($(this).hasClass("video")){
			readImage(this, "video");
		}
  });

  function readImage(input, type) {
    if ( input.files && input.files[0] ) {
      var FR= new FileReader();
      FR.onload = function(e) {
        var t = e.target.result;
        sendToPelars(t.split(",")[1], type);
      };
      FR.readAsDataURL( input.files[0] );
    }
  }

  function sendScoreToPlears(data, type){
    var url = "http://pelars.sssup.it/pelars/session/"+pelars.id+"?token="+token;
    var formData = {};
    /*
    if (type == "description"){
      formData.description = data;
    } else if (type == "score"){
      formData.score = data;
    }*/

    formData[type] = data;
    console.log(formData);

    $.ajax({
      type: "POST",
      url: url,
      data: JSON.stringify(formData),
      success: function(){console.log("yeah")},
      dataType: "json",
      contentType : "application/json; charset=utf-8"
    });
  }

  function sendToPelars(data,type){
		if (phase == "rate" || phase == "obs_rate" ){
			console.log(phase);
		} else {
			var url = "http://pelars.sssup.it/pelars/multimedia?token="+token;
			var formData = {
				"data": data,
				"id": pelars.id,
				"phase": phase,
				"time": Date.now(),
				"view": "mobile",
				"creator": creator
			};

			if (type == "text" || type == "choice"){
				var base64 = b64EncodeUnicode(data);
				formData.type = "text";
				formData.mimetype = "plain";
			} else if (type == "image"){
				formData.type = "image";
				formData.mimetype = "jpeg";
			} else if (type == "video"){
				formData.type = "video";
				formData.mimetype = "mp4";
			}

			$.put(url,JSON.stringify(formData),function(result){
				console.log(result);
			});
		}
  }
});

function b64EncodeUnicode(str) {
  return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function(match, p1) {
    return String.fromCharCode('0x' + p1);
  }));
}

function _ajax_request(url, data, callback, method) {
  return jQuery.ajax({
    url: url,
    type: method,
    data: data,
    success: callback
  });
}

jQuery.extend({
  put: function(url, data, callback) {
    return _ajax_request(url, data, callback, 'PUT');
  }});

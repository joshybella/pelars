/**
 * Created by joshy on 12/9/14.
 */
var GROUP = "<div><p>Group: REPLACE</p></div>";
var IMAGE = "<div class='info-window-image'><image src='REPLACE'></div>";
var NUMERICAL = "<div><p>Numerical: REPLACE</p></div>";
var DATE = "<div><p>Date: REPLACE</p></div>";
var TEXT = "<div><p>Text: REPLACE</p></div>";

var visualization = angular.module('Visualization', ['uiGmapgoogle-maps']);

visualization
  .config(function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
      //key: 'AIzaSyBdUO4IupvYhgqmQyTUkCHlIewdYpOCWHM',
      v: '3.17',
      libraries: 'weather,geometry,visualization'
    });
  })

  .controller('VisCtrl', function($scope, uiGmapGoogleMapApi){
    $scope.groups = groupnames;
    $scope.scenariodata = scenariodata;
    $scope.scenarioname = scenario.title;

    $scope.screens = [];
    $scope.scr = {};

    $scope.markers = {};
    $scope.maps = {};
    $scope.graph = {};
    $scope.media = {};
    $scope.text = {};

    /**
     * Filter out the amount of unique screens and store them in a list
     */
    scenariodata.forEach(function(item){
      var obj = Object.keys(item.data);

      obj.forEach(function(entry, i){
        if ($scope.screens.indexOf(entry) === -1){
          $scope.screens.push(entry);

          //create maps for every screen entry

          var mapname = "map"+(i+1),
              markername = "markers"+(i+1),
              graphname = "graph"+(i+1),
              medianame = "media"+(i+1),
              textname = "text"+(i+1);

          $scope.markers[markername] = [];
          $scope.graph[graphname] = [];
          $scope.media[medianame] = [];
          $scope.text[textname] = [];


          $scope.scr[mapname] = {
            name : mapname

          };

          $scope.maps[mapname] = {
            center: { latitude: 56.855670, longitude: 14.828869 },
            zoom: 14,
            bounds: {},
            id: i
          };
        }
      });
    });

    $scope.getBounds = function(i) {
      var mapname = "map"+(i+1);
      return $scope.maps[mapname]["bounds"];
    };

    $scope.getCenter = function(i) {
      var mapname = "map"+(i+1);
      return $scope.maps[mapname]["center"];
    };

    /*
    $scope.getZoom = function(i) {
      var mapname = "map"+(i+1);
      return $scope[mapname]["zoom"];
    };*/

    $scope.getMarkers = function(i) {
      var markername = "markers"+(i+1);
      return $scope.markers[markername];
    };

    $scope.getMedia = function(i) {
      var medianame = "media"+(i+1);
      return $scope.media[medianame];
    };

    $scope.getText = function(i) {
      var textname = "text"+(i+1);
      return $scope.text[textname];
    };

    $scope.hasMarkers = function(i) {
      var markername = "markers"+(i+1);
      return $scope.markers[markername].length > 0;
    };

    $scope.hasGraph = function(i) {
      var graphname = "graph"+(i+1);
      return $scope.graph[graphname];
    };

    $scope.hasMedia = function(i) {
      var medianame = "media"+(i+1);
      return $scope.media[medianame].length > 0;
    };

    $scope.hasText = function(i) {
      var textname = "text"+(i+1);
      return $scope.text[textname].length > 0;
    };

    // var createRandomMarker = function(i, bounds, idKey) {
    //   var lat_min = bounds.southwest.latitude,
    //     lat_range = bounds.northeast.latitude - lat_min,
    //     lng_min = bounds.southwest.longitude,
    //     lng_range = bounds.northeast.longitude - lng_min;

    //   if (idKey == null) {
    //     idKey = "id";
    //   }

    //   var latitude = lat_min + (Math.random() * lat_range);
    //   var longitude = lng_min + (Math.random() * lng_range);
    //   var ret = {
    //     latitude: latitude,
    //     longitude: longitude,
    //     title: 'm' + i
    //   };
    //   ret[idKey] = i;
    //   return ret;
    // };

    // $scope.randomMarkers = [];

    // $scope.$watch(function() {
    //   return $scope.map1.bounds;

    // }, function(nv, ov) {
    //   // Only need to regenerate once
    //   if (!ov.southwest && nv.southwest) {
    //     var markers = [];
    //     for (var i = 0; i < 3; i++) {
    //       markers.push(createRandomMarker(i, $scope.map1.bounds))
    //     }
    //     $scope.randomMarkers = markers;
    //   }
    // }, true);

    uiGmapGoogleMapApi.then(function(maps) {

      updateData($scope, "all");
      
    });

    $scope.chooseGroup = function(groupname) {
      // console.log(" clicked ", groupname);
      updateData($scope, groupname);
    }
  });

function updateData($scope, groupname) {
  //$scope.markers = {};
  var graphData = {};
  var graphGroups = {};
  var graphType = {};
  var timelineData = [];
  var timelineGroups = [];


  // TODO: LOOP ONLY ONCE!!!!!!!!!!!!!!!
  angular.forEach($scope.markers, function(value, key) {
    $scope.markers[key] = [];
  });

  angular.forEach($scope.graph, function(value, key) {
    $scope.graph[key] = false;
  });

  angular.forEach($scope.media, function(value, key) {
    $scope.media[key] = [];
  });

  angular.forEach($scope.text, function(value, key) {
    $scope.text[key] = [];
  });

  $scope.scenariodata.forEach(function(item, num) {                    // for-each submission
    
    if (groupname == "all" || groupname == item.groupname) {

      var d = item.data;

      // console.log("scenariodata - forEach: " + item);
      
      // console.log("original timestamp: " + item.timestamp);
      var timestamp = new Date(item.timestamp);
      timestamp.setHours(0);
      timestamp.setMinutes(0);
      timestamp.setSeconds(0);
      timestamp.setMilliseconds(0);
      // console.log("date timestamp: " + timestamp);
      timestamp = timestamp.getTime();
      // console.log("milli timestamp: " + timestamp);

      timelineData.push({label: item.groupname, times: [{"starting_time": timestamp, "ending_time": (timestamp + 3600)}]});
      timelineGroups.push(item.groupname);
      //console.log("d: ", d);

      // console.log("timelineData: ", timelineData);
      // console.log("timelineGroups: ", timelineGroups);

      $scope.screens.forEach(function(e, count) {          // for-each screen
        // console.log("\t$scope.screens - forEach: " + e);          
        if (d[e]){
          d[e].forEach(function(elem, countElem){                      // for each element on a screen
            // console.log("\t\td[e] - forEach: " + elem);
            // console.log("elem: ", elem);
            if (elem.type == "location"){
              var coords = elem.value;
              var values = [];
              var htmlValues = "";
              var val_list = [];

              d[e].forEach(function(ele, c){
                // console.log("\t\t\td[e] - forEach2: " + ele);
                //console.log("ele: ", ele);
                if (ele.type != "location"){
                  // values["value"+(c+1)] = {
                    // value: ele.value
                  if (ele.type === 'image') {
                    htmlValues += IMAGE.replace('REPLACE', ele.value);
                  } else if (ele.type === 'numerical') {
                    htmlValues += NUMERICAL.replace('REPLACE', ele.value);
                  } else if (ele.type === 'date') {
                    htmlValues += DATE.replace('REPLACE', ele.value);
                  } else if (ele.type == 'textarea' || ele.type == 'textinput') {
                    htmlValues += TEXT.replace('REPLACE', ele.value);
                  }

                  values.push({
                    type: ele.type,
                    data: ele.value
                  });
                  val_list.push("value"+(c+1));
                }
              });

              htmlValues = GROUP.replace('REPLACE', item.groupname) + htmlValues;

              var marker = {
                id: Math.random(),
                latitude: coords.split(";")[0], longitude: coords.split(";")[1],
                group: item.groupname,
                value: htmlValues,
                screen: e
              };//values

              val_list.forEach(function(el, ct){
                //marker["value"+(ct+1)]["value"] = el;
              });

              var markersname = "markers"+(count+1);

              // console.log("\t\t\t\tmarkersname: " + markersname);

              $scope.markers[markersname].push(marker);
            } else if (elem.type == "numerical") {
              // console.log("\t\t\t\tgraphData: " + elem.value);
              var graphname = "graph_"+(count+1)+"_"+(countElem+1);
              if (!graphData[graphname]) graphData[graphname] = [];
              if (!graphGroups[graphname]) graphGroups[graphname] = [];

              d[e].forEach(function(ele, c) {
                if (ele.type == "date") {
                  graphData[graphname][num] = {date: ele.value, value: parseInt(elem.value)};
                  graphGroups[graphname][num] = item.groupname;
                  if (!graphType[graphname]) graphType[graphname] = {diagramType: "scatterplot", valueType: "date-numerical"};
                } 
                // else if (ele.type == "numerical" && elem.elementId != ele.elementId) {
                //   if (!graphData[graphname][num]) {
                //     graphData[graphname][num] = [parseInt(ele.value), parseInt(elem.value)];
                //     graphGroups[graphname][num] = item.groupname;
                //     if (!graphType[graphname]) graphType[graphname] = {diagramType: "scatterplot", valueType: "numerical-numerical"};
                //   }
                // }
              });

              if (!graphData[graphname][num]) {
                graphData[graphname][num] = parseInt(elem.value);
                graphGroups[graphname][num] = item.groupname;
                if (!graphType[graphname]) graphType[graphname] = {diagramType: "barchart", valueType: "numerical"};
              }

              //graphData[graphname].push(parseInt(elem.value));
            } else if (elem.type == 'date') {
              var graphname = "graph_"+(count+1)+"_"+(countElem+1);
              if (!graphData[graphname]) {
                graphData[graphname] = [];
                graphGroups[graphname] = [];
                graphType[graphname] = {diagramType: "timeline", valueType: ""};
              }

              if (!graphData[graphname][num]) {
                var timestamp = new Date(elem.value);
                timestamp.setHours(0);
                timestamp.setMinutes(0);
                timestamp.setSeconds(0);
                timestamp.setMilliseconds(0);
                timestamp = timestamp.getTime();

                graphData[graphname][num] = {label: item.groupname, times: [{"starting_time": timestamp, "ending_time": (timestamp + 10)}]};
                graphGroups[graphname][num] = item.groupname;
              }
              // console.log(graphData[graphname]);

            } else if (elem.type == 'choice') {
              var graphname = "graph_"+(count+1)+"_"+(countElem+1);
              if (!graphData[graphname]) {
                graphData[graphname] = [];
                graphGroups[graphname] = ["not specified"];
                graphType[graphname] = {diagramType: "piechart", valueType: ""};

                var screenElements = scenario.screenData[count].screenElements;
                for (var i = 0; i < screenElements.length; i++) {
                  if (screenElements[i].type === 'choice') {
                    for (var j = 0; j < screenElements[i].content.length; j++) {
                      graphData[graphname].push({answer: screenElements[i].content[j]["[answer]"], value: 0});
                    }
                  }
                }
                // console.log(graphData[graphname]);
              }

              // if only one value was selected, the type to the value is a string, otherwise it's an array
              if (typeof elem.value === 'string' || elem.value instanceof String) {
                for (var j = 0; j < graphData[graphname].length; j++) {
                  if (graphData[graphname][j].answer == elem.value) {
                    graphData[graphname][j].value++;
                    break;
                  }
                }
              } else {
                for (var i = 0; i < elem.value.length; i++) {
                  for (var j = 0; j < graphData[graphname].length; j++) {
                    if (graphData[graphname][j].answer == elem.value[i]) {
                      graphData[graphname][j].value++;
                      break;
                    }
                  }
                }
              }
              // console.log(graphData[graphname]);

            } else if (elem.type == 'image' || elem.type == 'video' || elem.type == 'audio') {
              var medianame = "media"+(count+1);

              $scope.media[medianame].push({type: elem.type, value: elem.value, group: item.groupname})
            } else if (elem.type == 'textarea' || elem.type == 'textinput') {
              var textname = "text"+(count+1);

              $scope.text[textname].push({text: elem.value, group: item.groupname})
            }

          });
        }
      });
    }
  });
  
  var graphs = document.getElementsByClassName("graph");
  for (var i = 0; i < graphs.length; i++) {
    graphs[i].innerHTML = "";
  }

  angular.forEach(graphData, function(value, key) {
      graphData[key] = cleanArray(value);
      graphGroups[key] = cleanArray(graphGroups[key]);
      var graphname = key.substring(0, key.indexOf("_", 6));
      $scope.graph[graphname.replace("_", "")] = true;
      // console.log("key: " + key);
      // console.log("value: " + value);
      // document.getElementById(key).innerHTML = "";
      // console.log("#" + key.substring(0, key.indexOf("_", 6)));
      drawGraph("#" + graphname, graphType[key].diagramType, graphType[key].valueType, graphData[key], graphGroups[key], 600, 300, true, true, true);
  });

  // console.log(timelineData);
  // console.log(timelineGroups);

  document.getElementById("timeline_1").innerHTML = "";
  drawTimeline("#timeline_1", timelineData, timelineGroups, 600, 300);

}


/*
 * Removes all empty elements from an array and returns the new array
 */
function cleanArray(array) {
  var newArray = []
  for (var i = 0; i < array.length; i++) {
    if (array[i] !== undefined && array[i] !== null && array[i] !== "") {
      newArray.push(array[i]);
    }
  }
  return newArray;
}
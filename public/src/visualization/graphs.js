// var data1 = [ 38, 43, 35, 20, 35 ];

// var data2 = [
// 		[5, 20], [480, 90], [250, 50], [100, 33], [330, 95],
// 		[410, 12], [475, 44], [25, 67], [85, 21], [220, 88]
// 	];
// 
// var data3 = [{"date":"2012-03-20","value":3},{"date":"2012-03-21","value":8},{"date":"2012-03-22","value":2},{"date":"2012-03-23","value":10},{"date":"2012-03-24","value":3},{"date":"2012-03-25","value":20},{"date":"2012-03-26","value":12}];
// 
// $(function() {
	// drawGraph("scatterplot", "date-numerical", data3, 500, 300, 5, true, true, true);
	//drawGroupedBarChart();
	// drawPieChart();
// });
/**
  * draws the graph based on the passed values
  *
  * diagramType String 	- type of the diagram (e.g. "barchart" or "scatterplot")
  * valueType 	String 	- type of the values (e.g. "numerical", "numerical-numerical", "date-numerical")
  * dataset 	[] 		- array containing all the data values
  * width 		int 	- width of the diagram
  * height 		int 	- height of the diagram
  * padding 	float 	- padding of the diagram
  * xAxis 		boolean - true if the graph should draw an x-axis, false otherwise
  * yAxis 		boolean - true if the graph should draw an y-axis, false otherwise
  * tooltip 	boolean - true if the graph should display tooltips for the values, false otherwise
  **/
function drawGraph(htmlElement, diagramType, valueType, dataset, groups, width, height, xAxis, yAxis, tooltip) {
	switch(diagramType) {
		case "barchart": 
			switch(valueType) {
				case "numerical":
					drawBarChart(htmlElement, dataset, groups, width, height, xAxis, yAxis, tooltip);
					break;
				case "date-numerical":
					drawBarChartDateValue(htmlElement, dataset, groups, width, height, xAxis, yAxis, tooltip);
					break;
				default:
					console.log("Undefined value type");
			}
			break;
		case "scatterplot": 
			switch(valueType) {
				case "numerical-numerical":
					drawScatterPlotValueValue(htmlElement, dataset, groups, width, height, xAxis, yAxis, tooltip);
					break;
				case "date-numerical":
					drawScatterPlotDateValue(htmlElement, dataset, groups, width, height, xAxis, yAxis, tooltip);
					break;
				default:
					console.log("Undefined value type");
			}
			break;
		case "timeline":
			drawTimeline(htmlElement, dataset, groups, width, height);
			break;
		case "piechart":
			drawPieChart(htmlElement, dataset, width, height);
			break;
		default: 
			console.log("Undefined diagram type");
	}
}

//drawBarChart(data1);
//drawScatterPlotDateValue(data3);

//drawBarChartDateValue(data3);

function drawBarChart(htmlElement, dataset, groups, width, height, xAxis, yAxis, tooltip) {
	var margin = {top: 40, right: 40, bottom: 40, left:40};

	var uniqueGroups = uniqueArray(groups);
	var colors = [];
	for (var i = 0; i < uniqueGroups.length; i++) {
		colors.push(d3.scale.category20().range()[i % 20]);
	}
	var color = d3.scale.ordinal()
    			  .domain(uniqueGroups)
    			  .range(colors);

	var xScale = d3.scale.ordinal()
					.domain(d3.range(dataset.length))
					.rangeRoundBands([margin.left, width - margin.left - margin.right]);

	var yScale = d3.scale.linear()
					.domain([0, d3.max(dataset, function(d) { return d; })])
					.range([height - margin.top - margin.bottom, margin.bottom]);

	var svg = d3.select(htmlElement) 
				.append("svg")
            	.attr("width", width)
            	.attr("height", height);

    svg.selectAll("rect")
       	.data(dataset)
       	.enter()
       	.append("rect")
       	.attr("x", function(d, i) {
			return xScale(i);
		})
		.attr("y", function(d) {
			return yScale(d);
		})
		.attr("width", xScale.rangeBand())
		.attr("height", function(d) {
			return height - margin.top - margin.bottom - yScale(d);
		})
		.attr("fill", function(d, i) { return color(groups[i]); })
	
	svg.selectAll("text")
   		.data(dataset)
   		.enter()
		.append("text")
		.text(function(d) { 
			return d;
		})
		.attr("x", function(d, i) {
			return xScale(i) + xScale.rangeBand() / 2;
		})
		.attr("y", function(d) {
			return yScale(d) - 5;
		})
		.attr("font-family", "sans-serif")
			.attr("font-size", "11px")
			.attr("fill", "black")
			.attr("text-anchor", "middle");

	// if (xAxis) {
	//    	var xAxis = d3.svg.axis() 
	//    					.scale(xScale)
	//                   	.orient("bottom");
	//                   	//.ticks(5);

	//     svg.append('g')
	//     	.attr('class', 'axis')
	//     	.attr('transform', 'translate(0, ' + (height - margin.top - margin.bottom) + ')')
	//     	.call(xAxis);	

 //    }

    if (yAxis) {
        var yAxis = d3.svg.axis() 
        				.scale(yScale)
                      	.orient("left");
                      	//.ticks(5);
        svg.append("g")
        	.attr("class", "axis")
        	.attr("transform", "translate(" + margin.left + ",0)")
        	.call(yAxis);
    }
}

function drawBarChartDateValue(htmlElement, dataset, groups, width, height, xAxis, yAxis, tooltip) {
	var margin = {top: 40, right: 40, bottom: 40, left:40};

	var xScale = d3.time.scale()
	    		.domain([new Date(dataset[0].date), d3.time.day.offset(new Date(dataset[dataset.length - 1].date), 1)])
	    		.rangeRound([0, width - margin.left - margin.right]);

	var yScale = d3.scale.linear()
	    .domain([0, d3.max(dataset, function(d) { return d.value; })])
	    .range([height - margin.top - margin.bottom, 0]);

	var svg = d3.select(htmlElement)
				.append('svg')
	    		.attr('width', width)
	    		.attr('height', height)
	  			//.append('g')
	    		//.attr('transform', 'translate(' + margin.left + ', ' + margin.top + ')');

	svg.selectAll('rect')
	    .data(dataset)
	  	.enter()
	  	.append('rect')
	    .attr('x', function(d) { return xScale(new Date(d.date)); })
	    .attr('y', function(d) { return height - margin.top - margin.bottom - (height - margin.top - margin.bottom - yScale(d.value)) })
	    .attr('width', 10)
	    .attr('height', function(d) { return height - margin.top - margin.bottom - yScale(d.value) });

	if (xAxis) {
		var xAxis = d3.svg.axis()
		    .scale(xScale)
		    .orient('bottom')
		    .ticks(d3.time.days, 1)
		    .tickFormat(d3.time.format('%a %d'))
		    .tickSize(0)
		    .tickPadding(8);

		svg.append('g')
	    	.attr('class', 'axis')
	    	.attr('transform', 'translate(0, ' + (height - margin.top - margin.bottom) + ')')
	    	.call(xAxis);	
	}
	
	if (yAxis) {
		var yAxis = d3.svg.axis()
		    .scale(yScale)
		    .orient('left')
		    .tickPadding(8);

		svg.append('g')
		  	.attr('class', 'axis')
		  	.attr("transform", "translate(" + margin.left + ",0)")
		  	.call(yAxis);
	}
}

function drawScatterPlotValueValue(htmlElement, dataset, groups, width, height, xAxis, yAxis, tooltip) {
	var padding = 30;

	var xScale = d3.scale.linear()
					   .domain([0, d3.max(dataset, function(d) { return d[0]; })])
					   .range([padding, width - padding * 2]);
	var yScale = d3.scale.linear()
				   .domain([0, d3.max(dataset, function(d) { return d[1]; })])
				   .range([height - padding, padding]);
	var rScale = d3.scale.linear()
				   .domain([0, d3.max(dataset, function(d) { return d[1]; })])
				   .range([2, 5]);
	
	var uniqueGroups = uniqueArray(groups);
	var colors = [];
	for (var i = 0; i < uniqueGroups.length; i++) {
		colors.push(d3.scale.category20().range()[i % 20]);
	}
	var color = d3.scale.ordinal()
    			  .domain(uniqueGroups)
    			  .range(colors);

	var svg = d3.select(htmlElement) 
				.append("svg")
       			.attr("width", width)
        		.attr("height", height);

    svg.selectAll("circle")
    	.data(dataset)
		.enter()
		.append("circle")
		.attr("cx", function(d) { return xScale(d[0]); })
		.attr("cy", function(d) { return yScale(d[1]); })
		.attr("r",  function(d) { return rScale(d[1]); })
		//.attr("fill", "grey");
		.attr("fill", function(d, i) { return color(groups[i]); })
		.append("title") 
		.text(function(d, i) { return d[0] + "," + d[1] + "\n" + groups[i]; });

	// svg.selectAll("text")
	// 	.data(dataset)
	// 	.enter()
	// 	.append("text")
	// 	.text(function(d) {
	// 		return d[0] + "," + d[1];
	// 	})
	// 	.attr("x", function(d) { return xScale(d[0]); })
	// 	.attr("y", function(d) { return yScale(d[1]); })
	// 	.attr("font-family", "sans-serif")
 //   		.attr("font-size", "11px")
 //   		.attr("fill", "black");

   	if (xAxis) {
       	var xAxis = d3.svg.axis() 
       					.scale(xScale)
                      	.orient("bottom")
                      	.ticks(5);

        svg.append("g")
        	.attr("class", "axis")
        	.attr("transform", "translate(0," + (height - padding) + ")")
        	.call(xAxis);

    }

    if (yAxis) {
        var yAxis = d3.svg.axis() 
        				.scale(yScale)
                      	.orient("left")
                      	.ticks(5);
        svg.append("g")
        	.attr("class", "axis")
        	.attr("transform", "translate(" + padding + ",0)")
        	.call(yAxis);
    }

}

function drawScatterPlotDateValue(htmlElement, dataset, groups, width, height, xAxis, yAxis, tooltip) {
	var padding = 30;

	var dates = dataset.map(function(a) { return new Date(a.date); });
	var minDate = Math.min.apply(null, dates);
	var maxDate = Math.max.apply(null, dates);

	var xScale = d3.time.scale()
					.domain([new Date(minDate), new Date(maxDate)])
					.range([padding, width - padding * 2]);
	var yScale = d3.scale.linear()
					.domain([0, d3.max(dataset, function(d) { return d.value; })])
					.range([height - padding, padding]);

	var uniqueGroups = uniqueArray(groups);
	var colors = [];
	for (var i = 0; i < uniqueGroups.length; i++) {
		colors.push(d3.scale.category20().range()[i % 20]);
	}
	var color = d3.scale.ordinal()
    			  .domain(uniqueGroups)
    			  .range(colors);
	// var rScale = d3.scale.linear()
	// 				.domain([0, d3.max(dataset, function(d) { return d.value; })])
	// 				.range([2, 5]);

	var svg = d3.select(htmlElement) 
				.append("svg")
       			.attr("width", width)
        		.attr("height", height);

    svg.selectAll("circle")
    	.data(dataset)
		.enter()
		.append("circle")
		.attr("cx", function(d) { return xScale(new Date(d.date)); })
		.attr("cy", function(d) { return yScale(d.value); })
		.attr("r", 5)
		.attr("fill", function(d, i) { return color(groups[i]); })
		.append("title") 
		.text(function(d, i) { return d.value + "\n" + groups[i]; });

	// svg.selectAll("text")
	// 	.data(dataset)
	// 	.enter()
	// 	.append("text")
	// 	.text(function(d) {
	// 		return d.date + "," + d.value;
	// 	})
	// 	.attr("x", function(d) { return xScale(new Date(d.date)); })
	// 	.attr("y", function(d) { return yScale(d.value); })
	// 	.attr("font-family", "sans-serif")
//      		.attr("font-size", "11px")
//      		.attr("fill", "black");
	var diffDays = (maxDate - minDate) / (1000 * 60 * 60 * 24);
	var tickTime;
	var tickFormat;
	var tickInterval;
	if (diffDays > 365) {
		tickTime = d3.time.months;
		tickInterval = 3;
		format = d3.time.format("%Y-%m")
	} else if (diffDays > 28) {
		tickTime = d3.time.days;
		tickInterval = 12;
		tickFormat = d3.time.format("%m-%d")
	} else if (diffDays < 0) {
		tickTime = d3.time.hours;
		tickInterval = 1;
		tickFormat = d3.time.format("%H:%M");
	} else {
		tickTime = d3.time.days;
		tickInterval = 1;
		tickFormat = d3.time.format("%m-%d");
	} 



	if (xAxis) {
       	var xAxis = d3.svg.axis()
		    			.scale(xScale)
		    			.orient('bottom')
		    			.ticks(tickTime, tickInterval) //.ticks(d3.time.week, 1)
		    			.tickFormat(tickFormat); //.tickFormat(d3.time.format('%a %d'));

        svg.append("g")
        	.attr("class", "axis")
        	.attr("transform", "translate(0," + (height - padding) + ")")
        	.call(xAxis);
    }

    if (yAxis) {
        var yAxis = d3.svg.axis() 
        				.scale(yScale)
                      	.orient("left")
                      	.ticks(5);
        
        svg.append("g")
        	.attr("class", "axis")
        	.attr("transform", "translate(" + padding + ",0)")
        	.call(yAxis);
    }
}

function drawTimeline(htmlElement, dataset, groups, width, height) {
	//  var testData = [
	// 	{times: [{"starting_time": 1355752800000, "ending_time": 1355759900000}, {"starting_time": 1355767900000, "ending_time": 1355774400000}]},
	// 	{times: [{"starting_time": 1355759910000, "ending_time": 1355761900000}, ]},
	// 	{times: [{"starting_time": 1355761910000, "ending_time": 1355763910000}]},
	// ];

	var dates = dataset.map(function(a) { return a.times[0].starting_time; });
	var minDate = Math.min.apply(null, dates);
	var maxDate = Math.max.apply(null, dates);

	var diffDays = (maxDate - minDate) / (1000 * 60 * 60 * 24);

	// console.log("day difference: " + diffDays);
	var tickTime;
	var format;
	var tickInterval;
	if (diffDays > 365) {
		tickTime = d3.time.months;
		tickInterval = 3;
		format = d3.time.format("%Y-%m")
	} else if (diffDays > 28) {
		tickTime = d3.time.days;
		tickInterval = 12;
		format = d3.time.format("%m-%d")
	} else if (diffDays < 0) {
		tickTime = d3.time.hours;
		tickInterval = 1;
		format = d3.time.format("%H:%M");
	} else {
		tickTime = d3.time.days;
		tickInterval = 1;
		format = d3.time.format("%m-%d");
	} 

	var uniqueGroups = uniqueArray(groups);
	var colors = [];
	for (var i = 0; i < uniqueGroups.length; i++) {
		colors.push(d3.scale.category20().range()[i % 20]);
	}
	var color = d3.scale.ordinal()
    			  .domain(uniqueGroups)
    			  .range(colors);

	var chart = d3.timeline()
					.display("circle")
					.colors( color )
            		.colorProperty('label')
					.tickFormat(
						{format: format, //d3.time.format("%Y-%m-%d"), //%I %p
						tickTime: tickTime,
						tickInterval: tickInterval,
						tickSize: 10})
					.stack()
					.margin({left:150, right:50, top:50, bottom:0});
	
	var svg = d3.select(htmlElement)
				.append("svg")
				.attr("width", width)
				.attr("height", height)
				.datum(dataset)
				.call(chart);
}

function drawPieChart(htmlElement, dataset, width, height) {
	console.log("dataset: ", dataset);

	var radius = height/2;
	var color = d3.scale.category20c();

	var vis = d3.select(htmlElement).append("svg:svg").data([dataset]).attr("width", width).attr("height", height).append("svg:g").attr("transform", "translate(" + radius + "," + radius + ")");
	var pie = d3.layout.pie().value(function(d){return d.value;});

	// declare an arc generator function
	var arc = d3.svg.arc().outerRadius(radius);

	// select paths, use arc generator to draw
	var arcs = vis.selectAll("g.slice").data(pie).enter().append("svg:g").attr("class", "slice").filter(function(d) { return d.value > 0 });
	arcs.append("svg:path")
	    .attr("fill", function(d, i){
	        return color(i);
	    })
	    .attr("d", function (d) {
	        return arc(d);
	    });

	// add the text
	arcs.append("svg:text")
	    .attr("transform", function(d){
			d.innerRadius = 0;
			d.outerRadius = radius;
	    	return "translate(" + arc.centroid(d) + ")";
	    })
	    .attr("text-anchor", "middle").text( function(d, i) {
	    	return d.data.answer + ': ' + d.value;
	    });
}

// function drawGroupedBarChart() {
// 	// var mydata = [{group: "Bella's Angels", answers: [{answer: "A", selected: true}, {answer: "B", select: true}, {answer: "C", selected: false}, {answer: "D", selected: false}]},
// 	// 			  {group: "Bella's Devils", answers: [{answer: "A", selected: false}, {answer: "B", selected: false}, {answer: "C", selected: true}, {answer: "D", selected: true}]},
// 	// 			  {group: "Bella's Princesses", answers: [{answer: "A", selected: false}, {answer: "B", selected: true}, {answer: "C", selected: false}, {answer: "D", selected: true}]}];
// 	var data = [{group: "Bella's Angels", "A": true, "B": true, "C": false, "D": false},
// 				  {group: "Bella's Devils", "A": true, "B": false, "C": true, "D": true},
// 				  {group: "Bella's Princesses", "A": false, "B": true, "C": false, "D": true}];			  

// 	// var groups = ["Bella's Angels", "Bella's Devils", "Bella's Princesses"];

// 	var margin = {top: 20, right: 20, bottom: 30, left: 40},
//     width = 600 - margin.left - margin.right,
//     height = 200 - margin.top - margin.bottom;

// 	var x0 = d3.scale.ordinal()
// 	    .rangeRoundBands([0, width], .1);

// 	var x1 = d3.scale.ordinal();

// 	// var y = d3.scale.linear()
// 	//     .range([height, 0]);
// 	var y = d3.scale.ordinal()
// 			  .domain([false, true])
// 			  .range([height, 0]);

// 	var color = d3.scale.ordinal()
// 		.range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b"]);
// 	    // .range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);

// 	var xAxis = d3.svg.axis()
// 	    .scale(x0)
// 	    .orient("bottom");

// 	var yAxis = d3.svg.axis()
// 	    .scale(y)
// 	    .orient("left");
// 	    // .tickFormat(d3.format(".2s"));

// 	var svg = d3.select("body").append("svg")
// 	    .attr("width", width + margin.left + margin.right)
// 	    .attr("height", height + margin.top + margin.bottom)
// 	  .append("g")
// 	    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

// 	// d3.csv("/data.csv", function(error, data) {
// 	  //console.log(data);

// 	  //var ageNames = d3.keys(data[0]).filter(function(key) { return key !== "State"; });

// 	  var answerNames = d3.keys(data[0]).filter(function(key) { return key !== "group"; });

// 	  console.log("answerNames: " + answerNames); 

// 	  var colors = [];
// 	  for (var i = 0; i < answerNames.length; i++) {
// 	  	  colors.push(d3.scale.category20().range()[i % 20]);
//       }

//       console.log(colors);
// 	  var color = d3.scale.ordinal()
// 	    			.domain(answerNames)
// 	    			.range(colors);

// 	  // data.forEach(function(d) {
// 	  //   d.ages = ageNames.map(function(name) { return {name: name, value: +d[name]}; });
// 	  // });

// 	  data.forEach(function(d) {
// 	    d.answers = answerNames.map(function(name) { return {name: name, value: +d[name]}; });
// 	  });
// 	  // console.log(ageNames);
// 	  // console.log(data);
// 	  // console.log(data.map(function(d) { return d.State; })));

// 	  //x0.domain(data.map(function(d) { return d.State; }));
// 	  x0.domain(data.map(function(d) { return d.group; }));
// 	  // x1.domain(ageNames).rangeRoundBands([0, x0.rangeBand()]);
// 	  x1.domain(answerNames).rangeRoundBands([0, x0.rangeBand()]);
// 	  // y.domain([0, d3.max(data, function(d) { return d3.max(d.ages, function(d) { return d.value; }); })]);
// 	  //y.domain([0, d3.max(data, function(d) { return d3.max(d.answers, function(d) { return d.value; }); })]);

// 	  svg.append("g")
// 	      .attr("class", "x axis")
// 	      .attr("transform", "translate(0," + height + ")")
// 	      .call(xAxis);

// 	  svg.append("g")
// 	      .attr("class", "y axis")
// 	      .call(yAxis)
// 	    .append("text")
// 	      .attr("transform", "rotate(-90)")
// 	      .attr("y", 6)
// 	      .attr("dy", ".71em")
// 	      .style("text-anchor", "end")
// 	      .text("Population");

// 	  // var state = svg.selectAll(".state")
// 	  //     .data(data)
// 	  //   .enter().append("g")
// 	  //     .attr("class", "g")
// 	  //     .attr("transform", function(d) { return "translate(" + x0(d.State) + ",0)"; });

// 	  var group = svg.selectAll(".group")
// 	      .data(data)
// 	    .enter().append("g")
// 	      .attr("class", "g")
// 	      .attr("transform", function(d) { return "translate(" + x0(d.group) + ",0)"; });


// 	  // state.selectAll("rect")
// 	  group.selectAll("rect")
// 	      // .data(function(d) { return d.ages; })
// 	      .data(function(d) { return d.answers; })
// 	    .enter().append("rect")
// 	      .attr("width", x1.rangeBand())
// 	      .attr("x", function(d) { return x1(d.name); })
// 	      .attr("y", function(d) { return y(d.value); })
// 	      .attr("height", function(d) { return height - y(d.value); })
// 	      .style("fill", function(d) { return color(d.name); });

// 	  var legend = svg.selectAll(".legend")
// 	      // .data(ageNames.slice().reverse())
// 	      .data(answerNames.slice())
// 	    .enter().append("g")
// 	      .attr("class", "legend")
// 	      .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

// 	  legend.append("rect")
// 	      .attr("x", width - 18)
// 	      .attr("width", 18)
// 	      .attr("height", 18)
// 	      .style("fill", color);

// 	  legend.append("text")
// 	      .attr("x", width - 24)
// 	      .attr("y", 9)
// 	      .attr("dy", ".35em")
// 	      .style("text-anchor", "end")
// 	      .text(function(d) { return d; });

// 	// });
// }


function uniqueArray(array) {
  var n = {},r=[];
  for(var i = 0; i < array.length; i++) 
  {
    if (!n[array[i]]) 
    {
      n[array[i]] = true; 
      r.push(array[i]); 
    }
  }
  return r;
}